<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Add/Edit/Delete</title>
	<link rel="stylesheet" type="text/css" href="CSS/Basic.css" />
</head>
<body>
    <%@ include file = "/header.html" %>
	<h1>Add Edit Or Delete Client Information</h1>
	<form action="employeeActions" method="POST" >	
		<input type="hidden" name="hidden" value="update" />	
	    <c:forEach items="${clientInfo}" var="val">
	        <p>"${val.key}"</p><input type="text" name="${val.key}"  value="${val.value}" /><br />
	    </c:forEach>
	    <input type="submit" value="Update" />
	</form>
</body>
</html>