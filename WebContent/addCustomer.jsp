<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="CSS/Basic.css" />
	<title>Add Customer</title>
</head>
<body>
	<%@ include file = "/header.html" %>
	<h1>Add Customer Information</h1>
	<form id="addCustomerForm" action="managerActions" method="POST" >	
		<table>
		    <tr>
				<td>SSN: </td>
			    <td><input required type="text" name="SSN"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>Last Name: </td>
			    <td><input required type="text" name="LastName"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>First Name: </td>
			    <td><input required type="text" name="FirstName"  value="" /></td>
		   	</tr>
		   	
		   	<tr>
				<td>Address: </td>
			    <td><input required type="text" name="Address"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>ZipCode: </td>
			    <td><input required type="text" name="ZipCode"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>Telephone: </td>
			    <td><input required type="text" name="Telephone"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>UserName: </td>
			    <td><input required type="text" name="UserName"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>Password: </td>
			    <td><input required type="text" name="PasswordVal"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>Email </td>
			    <td><input required type="text" name="Email"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>Credit Card Number </td>
			    <td><input required type="text" name="CreditCardNumber"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>Rating </td>
			    <td><input required type="text" name="Rating"  value="" onblur="checkForm()"/></td>
		   	</tr>
	    </table>
	    <button type="submit" name="action" value="addCustomerInfo">Add Customer</button>
	    <button id="goback">Cancel</button>
	    <script>
	    	$(document).ready(function(){
				$("#goback").click(function(){
					parent.history.back();
					return false;
				});
			});
	    </script>
	    
	</form>
</body>
</html>