<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Customer Revenue Summary</title>

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<link rel="stylesheet" type="text/css" href="CSS/Basic.css" />
	<script>
		$(document).ready(function() {
			
			var customers = "${customers}";
			var revenues = "${revenues}";
			
			customers = customers.substring(0, customers.lastIndexOf(","));
			customers = customers.split(",");
			
			revenues = revenues.substring(0, revenues.lastIndexOf(","));
			revenues = revenues.split(",");
			revenues = revenues.map(Number);
			
			
			var chart = new Highcharts.Chart({
		        chart: {
		            type: 'bar',
		            renderTo: 'container'
		        },
		        title: {
		            text: 'Customer Revenue Summary'
		        },
		        
		        xAxis: {
		            categories: customers,
		            title: {
		                text: null
		            }
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Revenue',
		                align: 'high'
		            },
		            labels: {
		                overflow: 'justify'
		            }
		        },
		        tooltip: {
		            valuePrefix: '$'
		        },
		        plotOptions: {
		            bar: {
		                dataLabels: {
		                    enabled: true
		                }
		            }
		        },
		        credits: {
		            enabled: false
		        },
		
		        series: [{
		            name: 'Revenue',
		            data: revenues,
		            color: "#EF5350",
		        }]
		    });
		});
	</script>
</head>
<body>
	<%@ include file = "/header.html" %>
	<div id="container" style="width: 100%; height: 400px; padding:20px 0 0 0 "></div>
	<button id="goback">Go Back</button>
	    <script>
	    	$(document).ready(function(){
				$("#goback").click(function(){
					parent.history.back();
					return false;
				});
			});
	    </script>
</body>
</html>