<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>See Employee Info</title>

<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link type="text/css" rel="stylesheet" href="CSS/Basic.css">
</head>
<body>
	<script>
		var x = "${employeeList}";
	
	</script>
	<%
		String switchVal = (String) request.getSession().getAttribute("switch");
		// For returning
		String userName = (String) request.getSession().getAttribute("userName");
		String password = (String) request.getSession().getAttribute("password");
		String url = "/The_Red_Turnips/login?userName=" + userName + "&password=" + password + "&type=employee";
	%>
	
	<h3>Colleague Information</h3>
	<table>
		<tr>
			<th>Id</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Start Date</th>
			<th>Telephone</th>
		</tr>
		<c:forEach items="${employeeList}" var="current">
			<tr>
				<td><c:out value="${current.Id}" /></td>
				<td><c:out value="${current.FirstName}" /></td>
				<td><c:out value="${current.LastName}" /></td>
				<td><c:out value="${current.StartDate}" /></td>
				<td><c:out value="${current.Telephone}" /></td>
			</tr>
		</c:forEach>
	</table>
	<a style="float: right;" href=<%=url%>>Go Back</a>
	</body>
</html>