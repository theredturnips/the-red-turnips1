<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="CSS/Basic.css">
	<title>People Management</title>
	<style>
	footer {
		border: 1px solid grey;
		position: absolute;
		bottom: 0;
		width: 100%;
		height: 60px;
	}
	</style>
	<script>
		$(document).ready(function() {
			$(".editEmpBt").click(function(e){
				$("#selectedEmployee").attr("value", $(this).attr("name"))
				var $row = $(this).closest("tr");      
		    	var $tds = $row.children();             
				var str = ""
		    	$.each($tds, function() {    
		    	    str += $(this).text() + ",";
		    	})
		    	$("#selectedEmployeeInfo").attr("value", str);
				$("#testAction").attr("value", "editEmployee");
			})
		})
	</script>
	<script>
		$(document).ready(function() {
			$(".editCsmBt").click(function(e){
				alert($(this).attr("name"))
				$("#selectedCustomer").attr("value", $(this).attr("name"))
				$("#testAction1").attr("value", "deleteCustomer");
			})
		})
	</script>
	<script>
		$(document).ready(function() {
			$(".actionButton").click(function(e){
				$("#testAction").attr("value", $(this).attr("value"));
			});
			$(".actionButton1").click(function(e){
				$("#testAction1").attr("value", $(this).attr("value"));
			});
		})
	</script>
</head>

<body>
	<%@ include file="/header.html"%>
	<br>
	<form action="managerActions" method="GET">
		<button type="submit" name="action" value="stockManagement">Stock Management</button>
		<button type="submit" name="action" value="employeeManagement">People Management</button>
		<button type="submit" name="action" value="backupDatabase">Backup Database</button>
	</form>
	
	<div>
		<h3>Employee List</h3>
		<form action="/The_Red_Turnips/managerActions" method="GET">
			<input id="testAction" type="hidden" name="action" value="refreshEmployeeList" /> <input id="selectedEmployeeInfo" name="selectedEmployeeInfo" type="hidden" value="" />
			<table>
				<tr>
					<th>ID</th>
					<th>Last Name</th>
					<th>First Name</th>
					<th>SSN</th>
					<th>Address</th>
					<th>ZipCode</th>
					<th>Telephone</th>
					<th>StartDate</th>
					<th>HourlyRate</th>
					<th>Edit</th>
				</tr>
				<c:forEach items="${employeeList}" var="current">
					<tr>
						<td><c:out value="${current.Id}" /></td>
						<td><c:out value="${current.LastName}" /></td>
						<td><c:out value="${current.FirstName}" /></td>
						<td><c:out value="${current.SSN}" /></td>
						<td><c:out value="${current.Address}" /></td>
						<td><c:out value="${current.Zipcode}" /></td>
						<td><c:out value="${current.Telephone}" /></td>
						<td><c:out value="${current.StartDate}" /></td>
						<td><c:out value="${current.HourlyRate}" /></td>
						<td><button type="submit" class="editEmpBt" name="${current.Id}">Edit</button></td>
					</tr>
				</c:forEach>
			</table>
			<br>
			<button type="submit" class="actionButton" name="actionButton" value="addEmployee">Add Employee</button>
			<button type="submit" class="actionButton" name="refreshEmployeeList" value="refreshEmployeeList">Refresh Employee List</button>
		</form>
	</div>

	<hr>
	<br>
	<div>
		<h3>Customer List</h3>
		<form id="customerForm" action="/The_Red_Turnips/managerActions"
			method="GET">
			<input id="testAction1" type="hidden" name="action" value="refreshCustomerList" /> 
			<input id="selectedCustomer" name="selectedCustomer" type="hidden" value="" />
			<table>
				<tr>
					<th>ClientId</th>
					<th>Last Name</th>
					<th>First Name</th>
					<th>SSN</th>
					<th>Address</th>
					<th>ZipCode</th>
					<th>Telephone</th>
					<th>Email</th>
					<th>Credit Card Number</th>
					<th>Rating</th>
					<th>Edit</th>
				</tr>
				<c:forEach items="${customerList}" var="current">
					<tr>
						<td><c:out value="${current.Id}" /></td>
						<td><c:out value="${current.LastName}" /></td>
						<td><c:out value="${current.FirstName}" /></td>
						<td><c:out value="${current.SSN}" /></td>
						<td><c:out value="${current.Address}" /></td>
						<td><c:out value="${current.Zipcode}" /></td>
						<td><c:out value="${current.Telephone}" /></td>
						<td><c:out value="${current.Email}" /></td>
						<td><c:out value="${current.CreditCardNumber}" /></td>
						<td><c:out value="${current.Rating}" /></td>
						<td><button type="submit" class="editCsmBt" name="${current.Id}">Delete</button></td>
					</tr>
				</c:forEach>
			</table>
			<br>
			<button type="submit" class="actionButton1" name="actionButton" value="addCustomer">Add Customer</button>
			<button type="submit" class="actionButton1" name="refreshCustomerList" value="refreshCustomerList">Refresh Customer List</button>
		</form>

	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br><br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
</body>
</html>