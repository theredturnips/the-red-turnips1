<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Employee</title>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link type="text/css" rel="stylesheet" href="CSS/Basic.css">
		
		<script>
			$(document).ready(function() {
					// jquery is accessed before servlet, so can update hidden field
				     $('#clients').on('click','button', function(){
				    	 var $row = $(this).closest("tr");      
				    	 var $tds = $row.children();             
						 var str = ""
				    	$.each($tds, function() {    
				    	    str += $(this).text() + ",";
				    	})
				    	$("#test").val(str);
				});
					
					var x = "${suggestionLabel}";
					$("#suggestionLabel").text(x);
			});
		
		</script>
	</head>
	<body>	
		<%@ include file = "/headerEmployee.html" %>	
			 <div id="mailingList" >
			 	<form action="/The_Red_Turnips/employeeActions" method="GET">
			 		<input type="hidden" name="hidden" value="mailingList" />
			 		<h3>Get Mailing List</h3>
					 <table>
					 	<tr>
					 		<th>First Name</th>
					 		<th>Last Name</th>
					 		<th>Email Address</th>
					 		<th>Address</th>
					 		<th>Zip</th>
					 	</tr>
					  <c:forEach items="${mailingList}" var="current">
					    <tr>
					      <td><c:out value="${current.first}" /></td>
					      <td><c:out value="${current.last}" /></td>
					      <td><c:out value="${current.email}" /></td>
					      <td><c:out value="${current.address}" /></td>
					      <td><c:out value="${current.zip}" /></td>
					    </tr>
					  </c:forEach>
					</table>
					<button type="submit" value="Mailing List">Mailing List</button>
			 	</form>
			 </div>
			 
			 <hr>
	  		<div>
	  			<form action="employeeActions" method="GET">
		  			<h3>Produce Stock Suggestion</h3>
		  			<input type="hidden" name="hidden" value="suggestions" />
		  			<label id=suggestionsLabel></label>
		  			Client ID: <input type="text" name="clientID" />
		  			<button type="submit" value="submit">Submit</button>
		  			<br>
	  			</form>
	  		</div>
	  		
	  		<hr>
			<div >
				<h3>Record an Order</h3>
				<form action="employeeActions" method="POST">
		  		    <input type="hidden" name="hidden" value="record" />
		  		    <table id="orderTable"> 
		  		    	<tr>
		  		    		<th>OP</th>
		  		    		<th>Order Id</th>	
		  		    		<th>Stock Symbol</th>	
		  		    		<th>NumShares</th>	
		  		    		<th>Client Id</th>
		  		    		<th>Client Acct</th>	
		  		    		<th>Due Price</th>	
		  		    		<th>Order Type</th>
		  		    		<th>Time Stamp</th>
		  		    		<th>Produce Receipt</th>
		  		    	</tr>
		  		    	<c:forEach items="${orders}" var="order">
		  		    		<tr>
		  		    			<td><input type="radio" name="check" value="${order.orderId}"/></td>
		  		    			<td>${order.orderId}</td>
		  		    			<td>${order.stockSymbol}</td>
		  		    			<td>${order.numShares}</td>
		  		    			<td>${order.clientID}</td>
		  		    			<td>${order.accNum}</td>
		  		    			<td>${order.priceType}</td>
		  		    			<td>${order.orderType}</td>
		  		    			<td>${order.dateTimeStamp}</td>
		  		    			<td><input type="checkbox" name="${order.orderId}" value="${order.orderId}"></td>
		  		    		</tr>
		  		    	</c:forEach>
		  		    </table>
		  		    <button type="submit" value="record order">Record Order</button>
				</form>
			</div>
			
			<hr>
			<div>
				<h3>Change Customer Information</h3>
				<form action="employeeActions" method="POST">
					<input type="hidden" name="hidden" value="change" />
					<input id="test" type="hidden" name="test" value="" />
					<table id='clients'>
						<tr>
							<th>ID</th>
							<th>Credit Card Number</th>
							<th>Email</th>
							<th>Rating</th>
							<th>Edit</th>
						</tr>
				       <c:forEach items="${clients}" var="item">
				       		<tr>
				       			<td>${item.getID()}</td>
				       			<td>${item.getCreditCardNumber()}</td>
				       			<td>${item.getEmail()}</td>
				       			<td>${item.rating}</td>
				       			<td><button>Edit</button>
				       		</tr>
					   </c:forEach>
					</table>
				</form>
			</div>
			
			<hr>
		<form action="employeeActions" method="GET">
			<h3>See Colleague Information</h3>
		    <input type="hidden" name="hidden" value="seeEmployees" />
		    <button type="submit" value="Submit">Check</button>
		</form>
		<br><br><br><br><br><br><br><br><br><br><br><br>
	</body>
</html>