<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Add/Edit/Delete</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="CSS/Basic.css">
	<title>Order History</title>
	<style>
		p{
			width:100px;
			display:inline-block;
		}
	</style>
</head>
<body>
    <%@ include file = "/header.html" %>
    
	<h3>Add Edit Or Delete Client Information</h3>
	<form action="employeeActions" method="POST" >	
		<input type="hidden" name="hidden" value="update" />	
	    
	    <table>
			<c:forEach items="${clientInfo}" var="val">
			    <tr>
					<td>${val.key}: </td>
				    <td><input type="text" name="${val.key}"  value="${val.value}" /></td>
			   	</tr>
		   	</c:forEach>
		</table>
		
	    <button type="submit" value="Update">Update</button>
	    <button id="goback">Cancel</button>
	</form>
	
	<script>
	    $(document).ready(function(){
			$("#goback").click(function(){
				parent.history.back();
				return false;
			});
		});
	</script>
</body>
</html>