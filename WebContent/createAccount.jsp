<!DOCTYPE HTML>
<html>
	<head>
		<title>Create An Account</title>
			<link rel="stylesheet" type="text/css" href="CSS/Basic.css" />
		<style>
			input {
				float: right;
			}
		</style>
	</head>
	<body>
		<%@ include file = "header.html" %>
		<h2>Create An Account</h2>
		<div id="container" style="width:40%">
			<form action="createAccount" method="POST">
				<div class="formContainer">
				First Name*: <input type="text" name="firstname" /><br />
				</div>
				<div class="formContainer">
				Last Name*:  <input type="text" name="lastname" /><br />
				</div>
				<div class="formContainer">
				E-mail*: <input type="text" name="email" /><br />
				</div>
				<div class="formContainer">
				SSN*: <input type="text" name="SSN" /><br />
				</div>
				<div class="formContainer">
				Address: <input type="text" name="address"/><br />
				</div>
				<div class="formContainer">
				ZipCode: <input type="text" name="zip"/><br />
				</div>
				<div class="formContainer">
				Telephone: <input type="text" name="telephone"/><br />
				</div>
				<div class="formContainer">
				Credit Card #: <input type="text" name="creditcard"/><br />
				</div>
				<div class="formContainer">
				Password: <input type="password" name="password" /><br />
				</div>
				<br />
				<input type="submit" value="submit" />
			</form>
		</div>
	</body>
</html>