<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="../CSS/Basic.css">
	<title>How to make Suggestions</title>
</head>
<body>
	<%
		String switchVal = (String) request.getSession().getAttribute("switch");
		// For returning
		String userName = (String) request.getSession().getAttribute("userName");
		String password = (String) request.getSession().getAttribute("password");
		String url = "/The_Red_Turnips/login?userName=" + userName + "&password=" + password + "&type=employee";
	%>
	<h3 style="color:#EF5350; ">How To: Produce a Suggestion List</h3>
	<p ">Each customer can receive a personalized suggestion list based on the previous types of stocks they have traded.
	To create a personalized suggestion list enter the client ID under "Create Suggestion List" and press enter. A client will
	not be able to see their suggestion list until a customer-representative creates one for them.</p>
	<img alt="help-suggestion" src="../images/help-suggestion.png">
	<p>The stock suggestion list will be produced based upon the previous types of stocks the customer has purchased</p>
	
	<button id="goback">Cancel</button>
	<script>
	    $(document).ready(function(){
			$("#goback").click(function(){
				parent.history.back();
				return false;
			});
		});
	</script>
</body>
</html>