<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Help: Account</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="../CSS/Basic.css">
</head>
<body>
	<%
		String switchVal = (String) request.getSession().getAttribute("switch");
		// For returning
		String userName = (String) request.getSession().getAttribute("userName");
		String password = (String) request.getSession().getAttribute("password");
		String url = "/The_Red_Turnips/login?userName=" + userName + "&password=" + password + "&type=client";
	%>
		<h3 style="color:#EF5350; ">How To: Use Your Accounts</h3>
	<p>A client is allowed to hold multiple accounts to buy and sell stocks. To create a new account, simply press the button under 
	"Create New Account". With each account there is an associated stock portfolio that holds information about
	your stock holdings. This information will be displayed on the top of the page after logging in. To select an account, choose one of your accounts with through the dropdown menu at the top of the page 
	after logging in. All orders will be automatically be placed under that account.</p>
    
	<button id="goback">Cancel</button>
	<script>
	    $(document).ready(function(){
			$("#goback").click(function(){
				parent.history.back();
				return false;
			});
		});
	</script>
	
</body>
</html>