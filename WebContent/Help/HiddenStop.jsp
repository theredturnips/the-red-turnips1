<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="../CSS/Basic.css">
	<title>Help: Hidden Stop</title>
</head>
<body>
	<%
		String switchVal = (String) request.getSession().getAttribute("switch");
		// For returning
		String userName = (String) request.getSession().getAttribute("userName");
		String password = (String) request.getSession().getAttribute("password");
		String url = "/The_Red_Turnips/login?userName=" + userName + "&password=" + password + "&type=client";
	%>
		<h3 style="color:#EF5350;">How To: Place a Hidden Stop Order</h3>
	<p>A hidden stop order is a conditional order that remains hidden from the market. You select a stop price and if 
	the stock price per share falls below that price the hidden stop is executed at your selected stop price and sent to the
	market as a market order. The hidden stop order remains hidden from the market until the stock price falls below the 
	threshold.</p>
	<img src="hs.jpg" alt="Hidden Stop Picture" height="150" width="400" />
	<p>An illustration of a stop order occurring</p><br />
	
	<button id="goback">Cancel</button>
	<script>
	    $(document).ready(function(){
			$("#goback").click(function(){
				parent.history.back();
				return false;
			});
		});
	</script>
</body>
</html>