<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> 	
	<title>Help: Trailing Stop</title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="../CSS/Basic.css">
</head>
<body>
	<%
		String switchVal = (String) request.getSession().getAttribute("switch");
		// For returning
		String userName = (String) request.getSession().getAttribute("userName");
		String password = (String) request.getSession().getAttribute("password");
		String url = "/The_Red_Turnips/login?userName=" + userName + "&password=" + password + "&type=client";
	%>
	<h3 style="color:#EF5350;">How To: Place a Trailing Stop Order</h3>
	<p>
		Trailing Stops are conditional orders where the stop value follows the stock price. As the stock price rises 
		the trailing stop will follow behind it. As the stock price drops the trailing stop will remain in place. If 
		the stock drops below the stop amount the trailing stop will be triggered and the trailing stop order will be placed
		as a market order. Note stop prices can be in both dollar amounts or percentages; these types of orders are hidden from 
		the market until triggered.
	</p>
	<img src="ts.jpg" alt="Trailing Stop Picture" height="300" width="600" />
	<p>An illustration of a trailing stop order occurring</p><br />
	
	<button id="goback">Cancel</button>
	<script>
	    $(document).ready(function(){
			$("#goback").click(function(){
				parent.history.back();
				return false;
			});
		});
	</script>
</body>
</html>