<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="../CSS/Basic.css">
	<title>Help: Record Order</title>
</head>
<body>
	<%
		String switchVal = (String) request.getSession().getAttribute("switch");
		// For returning
		String userName = (String) request.getSession().getAttribute("userName");
		String password = (String) request.getSession().getAttribute("password");
		String url = "/The_Red_Turnips/login?userName=" + userName + "&password=" + password + "&type=employee";
	%>
	<h3 style="color:#EF5350;">How To: Record An Order</h3>
	<p>To record a market order select the appropriate order and click the submit button. To record a receipt for the customer
	check the column labeled "receipt" before recording an order.</p>
	<img alt="help-suggestion" src="../images/help-record.png">
	<p>An order being placed with the receipt option checked</p>
	
	<button id="goback">Cancel</button>
	<script>
	    $(document).ready(function(){
			$("#goback").click(function(){
				parent.history.back();
				return false;
			});
		});
	</script>
</body>
</html>