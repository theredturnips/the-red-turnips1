<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Stock Management</title>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link type="text/css" rel="stylesheet" href="CSS/Basic.css">
		
		<script>
			function hiddenField(action) {
				$("#hiddenField").val(action);
			}	
			function getOrder(stockSymbol){
				$("#selectOrder").val(action);
				window.open ("orderList.jsp")
			}
			function getOrderByCustom(){
				$("#selectCustom").val(action);
				window.open ("orderList.jsp")
			}
		</script>
	</head>
<body>
	<%@ include file = "header.html" %>
	<br>
	<form action="managerActions" method="GET">
		<button type="submit" name="action" value="stockManagement">Stock Management</button>
		<button type="submit" name="action" value="employeeManagement">People Management</button>
		<button type="submit" name="action" value="backupDatabase">Backup Database</button>
	</form>
	
	<br>
	<div>
        <form action="updateStock" method="POST" onSubmit="hiddenField('updatePrice')">
        	<h3>Update Stock Price</h3>
			<input name="hiddenField" id="hiddenField" type="hidden"  value="changePrice" />

			Stock Symbol: <input name="stockSymbol" type="text" placeholder="Ex.: GM"/> 
			New Price: <input name="newPrice" type ="text" placeholder="Ex.: 50.0"/> &nbsp;&nbsp;

			<button id="changePriceButton" type="submit" value="Update Stock Price" >Update Stock Price</button>	
		</form>

		<!-- stock list  -->
		<hr />
		<div class="stockList listStyle">
			<form action="managerActions" method="GET">
				<input id="stockAction" type="hidden" name="action" value="refreshStockList" />
				<input id="selectedStock" type="hidden" name="selectedStock" value="" />
				<h3 style="display:inline-block">Stock List</h3>
				<table>
					 <tr>
						<th>Stock Symbol</th>
					 	<th>CompanyName </th>
						<th>Stock Type </th>
						<th>Price Per Share </th>
				 		<th>Number of Shares </th>
				 		<th>Revenue </th>
				 		<th>Order History </th>
				 	</tr>
					<c:forEach items="${stock_List}" var="current">
						<tr>
							<td><c:out value="${current.StockSymbol}" /></td>
					      	<td><c:out value="${current.CompanyName}" /></td>
					    	<td><c:out value="${current.StockType}" /></td>
					    	<td><c:out value="${current.PricePerShare}" /></td>
					      	<td><c:out value="${current.NumShares}" /></td>
					     	<td><button type="submit" class="stockListAction" value="showStockRevenue" name="${current.StockSymbol}"> Show Revenue </button>
					     	<td><button type="submit" class="stockListAction" value="showStockOrderHistory" name="${current.StockSymbol}">Show History</button>
					     	
					    </tr>
					</c:forEach>
				</table>
				<br>
				<button type="submit" class="stockListAction" name="refreshStockList" value="refreshStockList">Refresh Stock List</button>
				<button type="submit" class="stockListAction" name="sortStockList" value="sortStockList">Sort According to # of Trades</button>
				<td><button type="submit" class="stockListAction" value="addStockPage"">Add Stock</button>
			</form>
			<script>
				$(document).ready(function() {
					$(".stockListAction").click(function(e){
						$("#stockAction").attr("value", $(this).attr("value"));
						$("#selectedStock").attr("value", $(this).attr("name"));
					});
				})
			</script>
		</div>
		
		<!-- customer list -->
		<hr />
		<div class="customerList listStyle">
			<form action="managerActions" method="GET">
				<input id="customerAction" type="hidden" name="action" value="refreshCustomerpage" />
				<input id="selectedCustomer" type="hidden" name="selectedCustomer" value="" />
				<h3 style="display:inline-block">Customer List</h3>
				<table>
					 <tr>
						<th>Custom ID </th> 
					 	<th>Credit Card Number </th>
						<th>Email </th>
						<th>Rating </th>
						<th>Revenue </th>
				 		<th>Order History </th>
				 	</tr>
					<c:forEach items="${clientList}" var="client">
						<tr>
							<td><c:out value="${client.Id}" /></td>
					      	<td><c:out value="${client.CreditCardNumber}" /></td>
					    	<td><c:out value="${client.Email}" /></td>
					    	<td><c:out value="${client.rating}" /></td>
					    	<td><button type="submit" class="customerListAction" value="showCustomerRevenue" name="${client.Id}"> Show Revenue </button>
					     	<td><button type="submit" class="customerListAction" value="showCustomerOrderHistory" name="${client.Id}">Show History</button>
					    </tr>
					</c:forEach>
				</table>
				<br>
				<button type="submit" class="customerListAction" name="refreshCustomerpage" value="refreshCustomerpage">Refresh Custom List</button>
			</form>
			<script>
				$(document).ready(function() {
					$(".customerListAction").click(function(e){
						$("#customerAction").attr("value", $(this).attr("value"));
						$("#selectedCustomer").attr("value", $(this).attr("name"));
					});
				})
			</script>
		</div>
		
		<!-- stock type list -->
		<hr />
		<div class="stickTypeList listStyle">
			<form action="managerActions" method="GET">
				<input id="stockTypeAction" type="hidden" name="action" value="refreshStockTypeList" />
				<input id="selectedStockType" type="hidden" name="selectedStockType" value="" />
				<h3 style="display:inline-block">Stock Type List</h3>
				<table>
					 <tr>
						<th>Stock Type </th> 
						<th>Revenue </th>
				 	</tr>
					<c:forEach items="${stockTypeList}" var="item">
						<tr>
							<td><c:out value="${item.StockType}" /></td>
					    	<td><button type="submit" class="stockTypeListAction" value="showStockTypeRevenue" name="${item.StockType}"> Show Revenue </button>
					   	</tr>
					</c:forEach>
				</table>
				<br>
				<button type="submit" class="stockTypeListAction" name="refreshStockTypeList" value="refreshStockTypeList">Refresh Stock Type List</button>
			</form>
			<script>
				$(document).ready(function() {
					$(".stockTypeListAction").click(function(e){
						$("#stockTypeAction").attr("value", $(this).attr("value"));
						$("#selectedStockType").attr("value", $(this).attr("name"));
					});
				})
			</script>
		</div>
		
		<hr />
		<!-- Determine which customer representative generated most total revenue(transaction fee)  -->
		<form action="managerActions" method="GET">
			<input id="moreOP" type="hidden" name="action" value="" />
			
			Customer representative generated most total revenue:
			<span style="color:#E53935">
				${bestEmployeeName[0].LastName} ${bestEmployeeName[0].FirstName}
			</span>
			<button type="submit" class="moreOP" name="refreshBestEmployee" value="refreshBestEmployee">Refresh</button>
			<br><br>
			
			Customer who generated most total revenue:
			<span style="color:#E53935">
				${bestCustomerName[0].LastName} ${bestCustomerName[0].FirstName}
			</span>
			<button type="submit" class="moreOP" name="refreshBestCustomer" value="refreshBestCustomer">Refresh</button>
			<br><br>
			
			Customer Revenue Summary:
			<button type="submit" class="moreOP" name="customerRevenueSummary" value="customerRevenueSummary">Check</button>
			<br><br>
			
			Obtain a sales report for a particular month: 
			<input name="selectedYear" type="text" value="" placeholder="Year" />
			<input name="selectedMonth" type="text" value="" placeholder="Month" />
			&nbsp; &nbsp;
			<button type="submit" class="moreOP" name="getSaleReport" value="getSaleReport"> View Report</button>
		</form>
		<script>
			$(document).ready(function() {
				$(".moreOP").click(function(e){
					$("#moreOP").attr("value", $(this).attr("value"));
				});
			})
		</script>
		
		<br><br><br><br><br><br><br><br><br>
	</div>
</body>
</html>