<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="page-header">
  		<h1>Buy or Sell Stocks</h1>
    </div>
		<form action="stockOrdering" method="POST" >
			<input type="hidden" name="hidden" value="market" />
			<div>
			Order Type
			<select class="form-group" name="orderType">
				<option value="buy">Buy</option>
				<option value="sell">Sell</option>
			</select>
			Quantity
			<input  type="text" name="quantity" />
			Symbol
			<input  type="text" name="symbol" />
			<br />
			Price Type
			<select class="form-group" name="priceType">
				<option value='Market'>Market</option>
				<option value='MarketOnClose'>Market On Close</option>
				<option value='TrailingStop'>Trailing Stop</option>
				<option value='HiddenStop'>Hidden Stop</option>
			</select>
			<input type="submit" value="Submit" name="submit" />
			</div>
		</form>
		
		<br />
		<form action="stockOrdering" method="POST" >
			<input type="hidden" name="hidden" value="conditional" />
			<div>
			Order Type
			<select class="form-group" name="orderType">
				<option value="buy">Buy</option>
				<option value="sell">Sell</option>
			</select>
			Quantity
			<input  type="text" name="quantity" />
			Symbol
			<input  type="text" name=symbol />
			<br />
			Stop Parameter
			<select class="form-group" name="priceType">
				<option value='TrailingStopDollar'>Trailing Stop $</option>
				<option value='TrailingStopPercent'>Trailing Stop %</option>
				<option value='HiddenStop'>Hidden Stop</option>
			</select>
			Stop Value
			<input type="text" name="stopValue" />
			<input type="submit" value="Submit" name="condSubmit" />
			</div>
		</form>

</body>
</html>