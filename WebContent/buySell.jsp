<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
	</head>
	<link type="text/css" rel="stylesheet" href="CSS/Basic.css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	
<body>
 
	<script>
    	$(document).ready(function() {
    		$("anything ul li a").click(function(e) {
				e.preventDefault();
				$("#mainframe").attr("src", $(this).attr("href"));
			});
		});
	</script>
	<div class="anything">
		<ul>
			<li><a href="marketOrder.jsp">Market</a></li>
			<li><a href="conditionalOrder.jsp">Conditional</a></li>
		</ul>
	</div>
	<iframe id="mainframe" class="mainframe" name="mainframe" src="marketOrder.jsp"></iframe>
	
	
</body>
</html>