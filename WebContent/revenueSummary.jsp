<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Revenue Summary</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="CSS/Basic.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
</head>

<body>
	<h2>Revenue Summary</h2>
	<div class="listStyle">
		<form action="managerActions" method="GET">
			<table>
				<tr>
					<th>Item </th>
					<th>Revenue </th>
				</tr>
				<c:forEach items="${revenueDisplayList}" var="item">
					<tr>
						<td><c:out value="${item.Name}" /></td>
						<td><c:out value="${item.Revenue}" /></td>
					</tr>
				</c:forEach>
			</table>
		</form>
	</div>
	<button id="goback">Go Back</button>
	    <script>
	    	$(document).ready(function(){
				$("#goback").click(function(){
					parent.history.back();
					return false;
				});
			});
	    </script>
	<hr>
</body>
</html>