<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="CSS/Basic.css" />
	<title>Edit Employee</title>
	<style>
		p{
			width:100px;
			display:inline-block;
		}
	</style>
	<script>
		$(document).ready(function() {
			$("#deleteEmployee").click(function(e){
				$("#test").attr("value", $(this).attr("value"));
			});
		})
	</script>
</head>
<body>
	<%@ include file = "/header.html" %>
	
	<h3>Edit Or Delete Employee Information</h3>
	<form action="managerActions" method="POST" >
		<input id="test" type="hidden" name="action" value="updateEmployeeInfo" />	
		<table>
			<c:forEach items="${emoloyeeInfo}" var="val">
			    <tr>
					<td>${val.key}: </td>
				    <td><input type="text" name="${val.key}"  value="${val.value}" /></td>
			   	</tr>
		   	</c:forEach>
		</table>
		<button type="submit" value="updateEmployeeInfo"> Update Employee Info</button>
	    <button id="deleteEmployee" value="deleteEmployee">Delete Employee</button>
	    <button id="goback">Cancel</button>
	</form>
	
	<script>
	    $(document).ready(function(){
			$("#goback").click(function(){
				parent.history.back();
				return false;
			});
		});
	</script>
</body>
</html>