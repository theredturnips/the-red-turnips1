<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Employee</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<style>
		table {
		    border-collapse: collapse;
		    width: 100%;
		}
		th, td {
		    text-align: left;
		    padding: 8px;
		}
		tr:nth-child(even){
			background-color: #f2f2f2;
		}
		th {
		    background-color: #3366ff;
		    color: white;
		}
	</style>
</head>

<body>
	<h1>Manager Page</h1>
	<div class="row">
		<div style="border: 1px solid black; id="stockList" class="col-sm-6">
			<form action="employeeActions" method="GET">
				<h3>Stock List</h3>
		 		<input type="hidden" name="hidden" value="stockList" />
				<table>
			 		<tr>
					 	<th>Stock Symbol</th>
					 	<th>Company Name</th>
						<th>Stock Type</th>
					 	<th>SharePrice</th>
					 	<th>NumShares</th>
				 	</tr>
					<c:forEach items="${stockList}" var="item">
				    	<tr>
					    	<td><c:out value="${item.StockSymbol}" /><td>
					      	<td><c:out value="${item.CompanyName}" /><td>
					      	<td><c:out value="${item.StockType}" /><td>
					      	<td><c:out value="${item.PricePerShare}" /><td>
						  	<td><c:out value="${item.NumShares}" /><td>
						</tr>
					 </c:forEach>
				</table>
			 	<input type="submit" value="Refresh stock List" />
			</form>
		</div>
			
	  	<div style="border: 1px solid black;" class="col-sm-6">
	  		<form action="employeeActions" method="GET">
		  			<h3>Produce Stock Suggestion</h3>
		  			<input type="hidden" name="hidden" value="suggestions" />
		  			Client ID: <input type="text" name="clientID" /><br />
		  			
		  			<input type="submit" value="Submit" />
	  			</form>
	  			<table>
					<tr>
						<th>Stock Symbol</th>
						<th>Stock Price</th>
						<th>Stock Type</th>
						<th>Number of Shares</th>
						<th>Company Name</th>
					</tr>
			       <c:forEach items="${stockSuggestions}" var="item">
			       		<tr>
			       			<td>${item.getStockSymbol()}</td>
			       			<td>${item.getPricePerShare()}</td>
			       			<td>${item.getStockType()}</td>
			       			<td>${item.getNumShares()}</td>
			       			<td>${item.getCompanyName()}</td>
			       		</tr>
				   </c:forEach>
				   </table>
	  		</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<h4>Record an Order: Check meaning</h4>
				<form action="employeeActions" method="POST">
		  		    <input type="hidden" name="hidden" value="record" />
				</form>
			</div>
			<div class="col-md-6">
			</div>
		</div>
</body>
</html>