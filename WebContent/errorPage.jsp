<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Sorry...</title>
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="CSS/Basic.css" />
</head>
<body>
	<%
		String switchVal = (String) request.getSession().getAttribute("switch");
		// For returning
		String userName = (String) request.getSession().getAttribute("userName");
		String password = (String) request.getSession().getAttribute("password");
		String type = (String) request.getSession().getAttribute("type");
		String url = "/The_Red_Turnips/login?userName=" + userName + "&password=" + password + "&type=" + type;
	%>
	
	<div style="text-align:center; display:block">	
		<img style="margin:auto" alt="error" src="images/error_pic.png">
		<div>
			There was an error. Please <span><a style="color:#EF5350" href=<%=url%>>Go Back</a> </span>and try later.
		</div>
	</div>
</body>
</html>