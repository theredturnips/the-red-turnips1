<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Add Stock</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="CSS/Basic.css">
</head>
<body>
	<h3>Add Stock</h3>
	<%@ include file = "/header.html" %>
	<form action="managerActions" method="POST" >	
		<table>
		    <tr>
				<td>StockSymbol: </td>
			    <td><input required type="text" name="StockSymbol"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>CompanyName: </td>
			    <td><input required type="text" name="CompanyName"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>StockType: </td>
			    <td><input required type="text" name="StockType"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>PricePerShare: </td>
			    <td><input required type="text" name="PricePerShare"  value="" /></td>
		   	</tr>
		   	<tr>
				<td>NumShares </td>
			    <td><input required type="text" name="NumShares"  value="" /></td>
		   	</tr>
		   
	    </table>
	    <button type="submit" name="action" value="addStock">Add Stock</button>
	    <button id="goback">Cancel</button>
	    <script>
	    	$(document).ready(function(){
				$("#goback").click(function(){
					parent.history.back();
					return false;
				});
			});
	    </script>
	    <br><br><br><br><br><br><br><br><br>
	</form>
</body>
</html>