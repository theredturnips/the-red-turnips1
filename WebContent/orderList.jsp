<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link type="text/css" rel="stylesheet" href="CSS/Basic.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<title>Order List</title>
</head>
<body>
	<div class="orderList listStyle">
		<h3 style="display:inline-block">Order List</h3>
		<ul>
			<li>
				<span class="li_span">Order ID </span>
				<span class="li_span">NumShares </span>
				<span class="li_span">Percentage </span>
				<span class="li_span">Price Per Share </span>
				<span class="li_span">Price </span>
				<span class="li_span">OrderType </span>
				<span class="li_span">DateTimeStamp </span>
		  	</li>
			<c:forEach items="${orderList}" var="item">
				<li>
					<span class="li_span"><c:out value="${item.getId()}" /> </span>
					<span class="li_span"><c:out value="${item.getNumShares()}" /> </span>
		       		<span class="li_span"><c:out value="${item.getPercentage()}" /> </span>
		       		<span class="li_span"><c:out value="${item.getPricePerShare()}" /> </span>
		       		<span class="li_span"><c:out value="${item.getPrice()}" /> </span>
		       		<span class="li_span"><c:out value="${item.getOrderType()}" /> </span>
		       		<span class="li_span"><c:out value="${item.getDateTime()}" /> </span>
		       		<span class="li_span">
		       			<input type="hidden" name="selectedStock" value="<c:out value="${item.getStockSymbol()}" />
		       			<input type="submit" value="show order" formaction="orderList" />" />	
		       		</span>
	      		</li>
			</c:forEach>
		</ul>
	</div>
</body>
</html>