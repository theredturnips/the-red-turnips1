<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Your Info</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<link rel="stylesheet" type="text/css" href="CSS/Basic.css" />
<script>
	
	function conditionalHistoryGraph(dates, stopPrices, prices, title) {
		dates = dates.substring(0, dates.lastIndexOf(","));
		var dates = dates.split(",");
		prices = prices.substring(0, prices.lastIndexOf(","));
		prices = prices.split(",");
		prices = prices.map(Number);
		stopPrices = stopPrices.substring(0, stopPrices.lastIndexOf(","));
		stopPrices = stopPrices.split(",");
		stopPrices = stopPrices.map(Number);

		
		var chart = new Highcharts.Chart({
			chart: {
                type: 'line',
                renderTo: 'container'
            },
	        title: {
	            text: title,
	            x: -20 //center
	        }, 
	        xAxis: {
	        	title: {
	        		text: 'Dates'
	        	},
	            categories: dates
	        },
	        yAxis: {
	            title: {
	                text: 'Price Per Share'
	            },
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#808080'
	            }]
	        },
	        tooltip: {
	            valuePrefix: '$'
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	       
	        series: [{
	            name: 'Price of Stock',
	            color: "#F00",
	            data: prices
	        }, {
	            name: 'Stop Price',
	            color: "#00F",
	            data: stopPrices
	        }]
	    });
		
	}
	
	function historyGraph(dates, prices, title) {
		dates = dates.substring(0, dates.lastIndexOf(","));
		var dates = dates.split(",");
		prices = prices.substring(0, prices.lastIndexOf(","));
		prices = prices.split(",");
		prices = prices.map(Number);
		
		var chart = new Highcharts.Chart({
			chart: {
                type: 'line',
                renderTo: 'container'
            },
	        title: {
	            text: title,
	            x: -20 //center
	        }, 
	        xAxis: {
	        	title: {
	        		text: 'Dates'
	        	},
	            categories: dates
	        },
	        yAxis: {
	            title: {
	                text: 'Price Per Share'
	            },
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#808080'
	            }]
	        },
	        tooltip: {
	            valuePrefix: '$'
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        series: [{
	            name: "Stock",
	            color: "#F00",
	            data: prices
	        }]
	    });

	}
	
	$(document).ready(function() {
		var switchVal = $('#test');
		var str = switchVal.text();

		if (str == "Trailing History") {
			$("#stockHistory").hide();
			$("#stockSuggestion").hide();
			$("#hiddenHistory").hide();
			$("#orderHistory").hide();
			$('#stockType').hide();
			$('#stockSearch').hide();
			$('#bestseller').hide();
			$('#Receipts').hide();
			// make line graph
			var dates = "${trailingDates}";
			var stopPrices = "${trailingStopPrices}";
			var prices = "${trailingPrices}";
			
			conditionalHistoryGraph(dates, stopPrices, prices, "Trailing History");
		   
		} else if (str == "Stock History") {
			$("#trailingHistory").hide();
			$("#stockSuggestion").hide();
			$("#hiddenHistory").hide();
			$("#orderHistory").hide();
			$('#stockType').hide();
			$('#stockSearch').hide();
			$('#bestseller').hide();
			$('#Receipts').hide();
			var dates = "${stockDates}";
			var prices = "${stockPrices}";
			
			historyGraph(dates, prices, "Stock History");
		} else if (str == "Stock Suggestions") {
			$("#stockHistory").hide();
			$("#trailingHistory").hide();
			$("#hiddenHistory").hide();
			$("#orderHistory").hide();
			$('#stockType').hide();
			$('#stockSearch').hide();
			$("#container").hide();
			$('#bestseller').hide();
			$('#Receipts').hide();
			var length = $("#suggestionTable > tbody > tr").length;
			if (length == 1) {
				$("#suggestionTable").hide();
				$("#suggLabel").text("An employee hasn't created a suggestion list for you yet");
			}
		} else if (str == "Hidden History") {
			$("#stockHistory").hide();
			$("#trailingHistory").hide();
			$("#stockSuggestion").hide();
			$("#orderHistory").hide();
			$('#stockType').hide();
			$('#stockSearch').hide();
			$('#bestseller').hide();
			$('#Receipts').hide();
			var dates = "${hiddenDates}";
			var stopPrices = "${hiddenStopPrices}";
			var prices = "${hiddenPrices}";
			
			conditionalHistoryGraph(dates, stopPrices, prices, "Hidden History");
		} else if (str == "Order History") {
			$("#stockHistory").hide();
			$("#trailingHistory").hide();
			$("#stockSuggestion").hide();
			$("#hiddenHistory").hide();
			$('#stockType').hide();
			$('#stockSearch').hide();
			$("#container").hide();
			$('#bestseller').hide();
			$('#Receipts').hide();
		} else if (str == "Stocks By Type") {
			$("#stockHistory").hide();
			$("#trailingHistory").hide();
			$("#stockSuggestion").hide();
			$("#hiddenHistory").hide();
			$('#orderHistory').hide();
			$('#stockSearch').hide();
			$("#container").hide();
			$('#bestseller').hide();
			$('#Receipts').hide();
		} else if (str == "Stocks By Keyword") {
			$("#stockHistory").hide();
			$("#trailingHistory").hide();
			$("#stockSuggestion").hide();
			$("#hiddenHistory").hide();
			$('#orderHistory').hide();
			$('#stockType').hide();
			$("#container").hide();
			$('#bestseller').hide();
			$('#Receipts').hide();			
		} else if (str == "Best Seller List"){
			$("#stockHistory").hide();
			$("#trailingHistory").hide();
			$("#stockSuggestion").hide();
			$("#hiddenHistory").hide();
			$('#orderHistory').hide();
			$('#stockType').hide();
			$('#stockSearch').hide();
			$("#container").hide();
			$('#Receipts').hide();
		} else if (str == "Receipts"){
			$('#stockSearch').hide();
			$("#stockHistory").hide();
			$("#trailingHistory").hide();
			$("#stockSuggestion").hide();
			$("#hiddenHistory").hide();
			$('#orderHistory').hide();
			$('#stockType').hide();
			$("#container").hide();
			$('#bestseller').hide();
		}
	});
</script>
</head>
<body>
	<%@ include file="/header.html"%>
	<%
		String switchVal = (String) request.getSession().getAttribute("switch");
		// For returning
		String userName = (String) request.getSession().getAttribute("userName");
		String password = (String) request.getSession().getAttribute("password");
		String url = "/The_Red_Turnips/login?userName=" + userName + "&password=" + password + "&type=client";
	%>
	<h2>
		<label id="test"><%=switchVal%></label>
	</h2>
	<a style="float: right;" href=<%=url%>>Go Back</a>
	
	<div id="container" style="width: 100%; height: 400px;"></div>
	<br/>
	<div style="margin-top: 20px;" id="trailingHistory">
		<table>
			<tr>
				<th>Order ID</th>
				<th>Price Per Share</th>
				<th>Stop Amount</th>
				<th>Date Time Stamp</th>
			</tr>
			<c:forEach items="${trailingHistory}" var="order">
				<tr>
					<td>${order.OrderId}</td>
					<td>${order.PricePerShare}</td>
					<td>${order.stopAmount}</td>
					<td>${order.DateTimeStamp}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<div id="hiddenHistory">
		<table>
			<tr>
				<th>Order ID</th>
				<th>Price Per Share</th>
				<th>Stop Amount</th>
				<th>Date Time Stamp</th>
			</tr>
			<c:forEach items="${hiddenHistory}" var="order">
				<tr>
					<td>${order.OrderId}</td>
					<td>${order.PricePerShare}</td>
					<td>${order.stopAmount}</td>
					<td>${order.DateTimeStamp}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<div id="stockSuggestion">
		<label id="suggLabel"></label>
		<table id="suggestionTable">
			<tr>
				<th>Stock Symbol</th>
				<th>Company Name</th>
				<th>Stock Type</th>
				<th>Price Per Share</th>
				<th>Number Of Shares</th>
			</tr>
			<c:forEach items="${suggestions}" var="stock">
				<tr>
					<td>${stock.StockSymbol}</td>
					<td>${stock.CompanyName}</td>
					<td>${stock.StockType }</td>
					<td>${stock.PricePerShare}</td>
					<td>${stock.NumShares}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<div id="stockHistory">
		<table>
			<tr>
				<th>Stock Symbol</th>
				<th>Price Per Share</th>
				<th>Date Time Stamp</th>
			</tr>
			<c:forEach items="${stockHistory}" var="stock">
				<tr>
					<td>${stock.StockSymbol}</td>
					<td>${stock.PricePerShare}</td>
					<td>${stock.DateTimeStamp}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<div id="orderHistory">
		<table>
			<tr>
				<th>Order ID</th>
				<th>Stock Symbol</th>
				<th>Price Type</th>
				<th>Date Time Stamp</th>
			</tr>
			<c:forEach items="${orderHistory}" var="order">
				<tr>
					<td>${order.orderId}</td>
					<td>${order.stockSymbol}</td>
					<td>${order.priceType}</td>
					<td>${order.dateTimeStamp}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<div id="stockSearch">
	<table>
		<tr>
			<th>Stock Symbol</th>
			<th>Company Name</th>
			<th>Stock Type</th>
			<th>Price Per Share</th>
			<th>Number of Shares</th>
		</tr>
		<c:forEach items="${stockSearches}" var="key">
			<tr>
				<td>${key.StockSymbol}</td>
				<td>${key.CompanyName}</td>
				<td>${key.StockType}</td>
				<td>${key.PricePerShare}</td>
				<td>${key.NumShares}</td>
			</tr>
		</c:forEach>
		</table>
	</div>
	<div id ="bestseller">
	<table>
		<tr>
			<th>Stock Symbol</th>
			<th>Amount</th>
		</tr>
		<c:forEach items="${bestsellerlist}" var="key">
			<tr>
				<td>${key.StockSymbol}</td>
				<td>${key.Amount}</td>
			</tr>
		</c:forEach>
	</table>
	</div>
	<div id ="Receipts">
	<table>
		<tr>
			<th>Client ID</th>
			<th>Stock Symbol</th>
			<th>Number of Shares</th>
			<th>Order Type</th>
			<th>Date of Order</th>
		</tr>
		<c:forEach items="${Receipts}" var="key">
			<tr>
				<td>${key.ClientID}</td>
				<td>${key.StockSymbol}</td>
				<td>${key.NumShares}</td>
				<td>${key.OrderType}</td>
				<td>${key.DateTimeStamp}</td>
			</tr>
		</c:forEach>
	</table>
	</div>
	<div id="stockType">
	<% try { %>
	<table>
			<tr>
				<th>Stock Symbol</th>
				<th>Company Name</th>
				<th>Stock Type</th>
				<th>Price Per Share</th>
				<th>Number of Shares</th>
			</tr>
			<c:forEach items="${stockTypes}" var="type">
				<tr>
					<td>${type.StockSymbol}</td>
					<td>${type.CompanyName}</td>
					<td>${type.StockType}</td>
					<td>${type.PricePerShare}</td>
					<td>${type.NumShares}</td>
				</tr>
			</c:forEach>
			</table>
	<% } catch (Exception e) {
		}%>
	</div>
	
	<br/><br/><br/>
</body>
</html>