<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Employee</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="CSS/Basic.css">
	<script>
		$(document).ready(function() {
			var select = $("#account");
			var test = $(select).val();
			$("#hiddenAccountMarket").val(test);
			$("#hiddenAccountConditional").val(test);
			$("#currentAccount").text("You're On Account " + test + " , you can change to account ");
	
			$("#account").change(function() {
				var select = $("#account");
				var test = $(select).val();
				$("#hiddenAccountMarket").val(test);
				$("#hiddenAccountConditional").val(test);
				$("#currentAccount").text("You're On Account " + test + " , you can change to account");
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("select").css("background-color", "#EF9A9A")	
		})
	</script>
</head>

<body>
	<%@ include file="/headerClient.html"%>
	<br>
	<label id="currentAccount"></label>
	<select id="account">
		<c:forEach items="${accNum}" var="acc">
			<option value="${acc}">${acc}</option>
		</c:forEach>
	</select>
	<form action="customerActions" method="POST">
		<input type="hidden" name="hidden" value="newAccount" />
		<h3>Create New Account: </h3>
		<button  type="submit" value="Submit"> New Account </button>
	</form>
	<hr>
	
	<h3>Current Stock Holdings</h3>
	<table>
		<tr>
			<th>Stock</th>
			<th>Number of Shares</th>
			<th>Account Number</th>
		</tr>
		<c:forEach items="${stocklist}" var="current">
			<tr>
				<td><c:out value="${current.stock}" /></td>
				<td><c:out value="${current.numShares}" /></td>
				<td><c:out value="${current.account}" /></td>
			</tr>
		</c:forEach>
	</table>
	
	<hr>
		<div>
			<h3>Buy or Sell Stocks</h3>
			<div>
				<h4>Market Order</h4>
				<form style="" action="stockOrdering" method="POST">
					<input type="hidden" name="hidden" value="market" />
					<div>
						Order Type 
						<select class="form-group" name="orderType">
							<option value="buy">Buy</option>
							<option value="sell">Sell</option>
						</select> 
						<br>
						Symbol   <input type="text" name="symbol" /> 
						Quantity <input type="text" name="quantity" /> Price Type 
						<select class="form-group" name="priceType">
							<option value='Market'>Market</option>
							<option value='MarketOnClose'>Market On Close</option>
						</select>
						
						<input id="hiddenAccountMarket" type="hidden" name="account" />
						<br />
						<button type="submit" value="Submit" name="submit" > Submit </button>
					</div>
				</form>
			</div>
			<br />
			<div>
				<h4>Conditional Order</h4>
				<form action="stockOrdering" method="POST">
					<input type="hidden" name="hidden" value="conditional" />
					<div>
						Order Type: 
						<select class="form-group" name="orderType">
							<option value="buy">Buy</option>
							<option value="sell">Sell</option>
						</select> 
						<br>
						Symbol:  <input type="text" name=symbol /> 
						Quantity:  <input type="text" name="quantity" /> 
						
						<br>
						Stop Parameter: 
						<select class="form-group" name="priceType">
							<option value='TrailingStopDollar'>Trailing Stop $</option>
							<option value='TrailingStopPercent'>Trailing Stop %</option>
							<option value='HiddenStop'>Hidden Stop</option>
						</select> 
						Stop Value: 
						<input type="text" name="stopValue" /> <br /> 
						<input id="hiddenAccountConditional" type="hidden" name="account" /> 
						<button type="submit" value="Submit" name="condSubmit" > Submit </button>
					</div>
				</form>
			</div>
		</div>
		<hr>
		
		<div>
			<form action="customerActions" method="POST">
				<h3>Trailing Stop History</h3>
				<input type="hidden" name="hidden" value="trailingHistory" />
				<table>
					<tr>
						<th>Select</th>
						<th>Order Id</th>
						<th>Stock Symbol</th>
						<th>NumShares</th>
					</tr>
					<c:forEach items="${trailingList}" var="order">
						<tr>
							<td><input type="radio" name="orderId"
								value="${order.orderId}" /></td>
							<td>${order.orderId}</td>
							<td>${order.StockSymbol}</td>
							<td>${order.NumShares}</td>
						</tr>
					</c:forEach>
				</table>
				<br>
				<button type="submit" value="Get History"> Get History </button>
			</form>
			<hr>
			
			<form action="customerActions" method="POST">
			<h3>Hidden Stop History</h3>
				<input type="hidden" name="hidden" value="hiddenHistory" />
				<table>
					<tr>
						<th>Select</th>
						<th>Order Id</th>
						<th>Stock Symbol</th>
						<th>NumShares</th>
					</tr>
					<c:forEach items="${hiddenList}" var="order">
						<tr>
							<td><input type="radio" name="orderId"
								value="${order.orderId}" /></td>
							<td>${order.orderId}</td>
							<td>${order.StockSymbol}</td>
							<td>${order.NumShares}</td>
						</tr>
					</c:forEach>
				</table>
				<br>
				<button type="submit" value="Get History"> Get History </button>
			</form>
			<hr>
			
			<form action="customerActions" method="GET">
				<h3>Stock Price History</h3>
				<input type="hidden" name="hidden" value="stockHistory" />
				<table>
					<tr>
						<th>Select</th>
						<th>Stock Symbol</th>
					</tr>
					<c:forEach items="${stockSymbol}" var="symbol">
						<tr>
							<td><input type="radio" name="symbol"
								value="${symbol.stockSymbol}" /></td>
							<td>${symbol.stockSymbol}</td>
						</tr>
					</c:forEach>
				</table>
				Num Months in Past: 
				<input type="text" name="numMonths" /> 
				<button type="submit" value="Get History"> Get History </button>
			</form>
			<hr>
			
			<form action="customerActions" method="GET">
				<input type="hidden" name="hidden" value="suggestions" /> 
				Stock Suggestions: 
				<button type="submit" value="Get Suggestions" >Get Suggestions</button>
			</form>
			<br>
			
			<form action="customerActions" method="GET">
				<input type="hidden" name="hidden" value="allOrders" />
				Get Order History
				<button type="submit" value="Get Order History" >Get Order History</button>
			</form>
			<br>
			
			<form action="customerActions" method="GET">
				<input type="hidden" name="hidden" value="bestSellerList">
				Best Seller
				<button type="submit" value="Submit" >Submit</button>
			</form>
			<br>
			
			<form action="customerActions" method="GET">
				<input type="hidden" name="hidden" value="receipts"> 
				Your Receipts
				<button type="submit" value="Submit" >Submit</button>
			</form>
			<br>
			
			<form action="customerActions" method="GET">
				<input type="hidden" name="hidden" value="stockType" />
				Stock By Type: 
				<select name="stockTypeList">
					<c:forEach items="${stockTypes}" var="type">
						<option value="${type}">${type}</option>
					</c:forEach>
				</select> 
				<button type="submit" value="Submit" >Submit</button>
			</form>
			<br>
			
			<form action="customerActions" method="GET">
				<input type="hidden" name="hidden" value="keyword" />
				Stock By Keyword(s)
				<input type="text" name="search" />
				<button type="submit" value="Submit" >Submit</button>
			</form>
		</div>
		<br><br><br><br><br><br><br>
</body>
</html>