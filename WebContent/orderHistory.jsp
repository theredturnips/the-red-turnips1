<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="CSS/Basic.css">
	<title>Order History</title>
</head>
<body>
	<div class="orderList listStyle">
		<h3 style="display:inline-block">Order List</h3>
		<table>
			<tr>
				<th>Order ID </th>
				<th>Client ID</th>
				<th>Stock Symbol</th>
				<th>NumShares </th>
				<th>AccNum </th>
				<th>Order Type </th>
				<th>Price Type</th>
				<th>Stop Value </th>
				<th>Date Time Stamp</th>
			</tr>
			<c:forEach items="${orderHistory}" var="current">
				<tr>
					<td><c:out value="${current.Id}" /></td>
			      	<td><c:out value="${current.ClientID}" /></td>
			    	<td><c:out value="${current.StockSymbol}" /></td>
					<td><c:out value="${current.NumShares}" /></td>
			      	<td><c:out value="${current.AccNum}" /></td>
			     	<td><c:out value="${current.OrderType}" /></td>
			     	<td><c:out value="${current.PriceType}" /></td>
			     	<td><c:out value="${current.stopValue}" /></td>
			     	<td><c:out value="${current.DateTimeStamp}" /></td>
			    </tr>
			</c:forEach>
		</table>
		<button id="goback">Go Back</button>
	    <script>
	    	$(document).ready(function(){
				$("#goback").click(function(){
					parent.history.back();
					return false;
				});
			});
	    </script>
	</div>
</body>
</html>