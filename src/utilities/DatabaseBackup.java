package utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DatabaseBackup {
	
	public static void main(String[] args){
		DatabaseBackup backUp = new DatabaseBackup();
		backUp.dbBackup();
	}
	
	public void dbBackup() {
		String executeCmd = "/usr/local/mysql/bin/mysqldump --opt -uroot -pjl8141994 db -r /Users/jameslynn/Desktop/CSE305Files/cse305Database.sql";
		Process runtimeProcess = null;
		try {
			//System.out.println(executeCmd);
			runtimeProcess = Runtime.getRuntime().exec(executeCmd);
			int processComplete = runtimeProcess.waitFor();

			if (processComplete == 0) {
				System.out.println("Backup created successfully");
			} else {
				System.out.println("Could not create the backup");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		try
        {
            InputStreamReader isr = new InputStreamReader(runtimeProcess.getErrorStream());
            BufferedReader br = new BufferedReader(isr,4094);
            String line=null;
            while ( (line = br.readLine()) != null)
                System.out.println("> " + line);    
            } catch (IOException ioe)
              {
                ioe.printStackTrace();  
              }
	}

}
