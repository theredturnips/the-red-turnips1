package beans;

public class Person {
	private String SSN; 
	private String LastName;
	private String FirstName;
	private String Address; 
	private int ZipCode;
	private int Telephone;
    private String PasswordVal;
	public String getSSN() {
		return SSN;
	}
	public void setSSN(String sSN) {
		SSN = sSN;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public int getZipCode() {
		return ZipCode;
	}
	public void setZipCode(int zipCode) {
		ZipCode = zipCode;
	}
	public int getTelephone() {
		return Telephone;
	}
	public void setTelephone(int telephone) {
		Telephone = telephone;
	}
	public String getPasswordVal() {
		return PasswordVal;
	}
	public void setPasswordVal(String passwordVal) {
		PasswordVal = passwordVal;
	}
    
    
}
