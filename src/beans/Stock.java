package beans;

public class Stock {
	private String StockSymbol;
	private String CompanyName;
	private String StockType;
	private double PricePerShare;
	private int NumShares;
	public String getStockSymbol() {
		return StockSymbol;
	}
	public void setStockSymbol(String stockSymbol) {
		StockSymbol = stockSymbol;
	}
	public String getCompanyName() {
		return CompanyName;
	}
	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}
	public String getStockType() {
		return StockType;
	}
	public void setStockType(String stockType) {
		StockType = stockType;
	}
	public double getPricePerShare() {
		return PricePerShare;
	}
	public void setPricePerShare(double pricePerShare) {
		PricePerShare = pricePerShare;
	}
	public int getNumShares() {
		return NumShares;
	}
	public void setNumShares(int numShares) {
		NumShares = numShares;
	}	
}
