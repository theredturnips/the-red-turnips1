package beans;

public class Order {
    private int id;
	private int NumShares;
	private String datetime;
	private double Percentage;
	private String ClientID;
	private String AccNum;
	private String StockSymbol;
	private double PricePerShare;
	private Price price;
	private OrderType orderType;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumShares() {
		return NumShares;
	}
	public void setNumShares(int numShares) {
		NumShares = numShares;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public double getPercentage() {
		return Percentage;
	}
	public void setPercentage(double percentage) {
		Percentage = percentage;
	}
	public String getClientID() {
		return ClientID;
	}
	public void setClientID(String clientID) {
		ClientID = clientID;
	}
	public String getAccNum() {
		return AccNum;
	}
	public void setAccNum(String accNum) {
		AccNum = accNum;
	}
	public String getStockSymbol() {
		return StockSymbol;
	}
	public void setStockSymbol(String stockSymbol) {
		StockSymbol = stockSymbol;
	}
	public double getPricePerShare() {
		return PricePerShare;
	}
	public void setPricePerShare(double pricePerShare) {
		PricePerShare = pricePerShare;
	}
	public Price getPrice() {
		return price;
	}
	public void setPrice(Price price) {
		this.price = price;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	
}
