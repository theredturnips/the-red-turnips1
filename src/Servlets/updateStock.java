package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.Statement;

import JDBC.Conn;

/**
 * Servlet implementation class updateStock
 */
@WebServlet("/updateStock")
public class updateStock extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public updateStock() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String str = request.getParameter("hiddenField");
		try {
			Connection conn = (Connection) Conn.getMySQLConnection();
			HttpSession session = request.getSession();
			Statement st = (Statement) conn.createStatement();
			String query;
			PreparedStatement pendingOrder = conn.prepareStatement("INSERT INTO PendingOrder(Id, NumShares, DateTimeStamp, stopValue, ClientID, AccNum, StockSymbol, PriceType, OrderType) VALUES "
					+ " (?,?,?,?,?,?,?,?,?)"); 
			PreparedStatement order = conn.prepareStatement("INSERT INTO OrderTable(Id, NumShares, DateTimeStamp, stopValue, ClientID, AccNum, StockSymbol, PriceType, OrderType) VALUES "
					+ " (?,?,?,?,?,?,?,?,?)"); 
			switch(str) {
			case "changePrice":
				String stockSymbol = request.getParameter("stockSymbol");
				double newPrice = Double.valueOf(request.getParameter("newPrice"));
				double oldPrice = 0.0;
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = new Date();
				String dateTimeStamp = dateFormat.format(date);
				
				query = "Select PricePerShare FROM StockTable WHERE StockSymbol= " + "\"" + stockSymbol + "\"";
				// IBM";update StockTable set PricePerShare=105 WHERE StockSymbol="IBM"; Select PricePerShare FROM StockTable WHERE StockSymbol= "IBM
				//System.out.println(query);
				ResultSet result = st.executeQuery(query);
				while (result.next()) {
					oldPrice = result.getDouble("PricePerShare");
				}
				
				// first update the price 
				query = "UPDATE StockTable Set PricePerShare=" + newPrice + " WHERE StockSymbol=" + "\"" + stockSymbol + "\"";
				st.executeUpdate(query);
			
				SimpleDateFormat stockDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				Date stockDate = new Date();
				String stockDateTimeStamp = stockDateFormat.format(stockDate);
				PreparedStatement stockHistory = conn.prepareStatement("INSERT INTO StockHistory(StockSymbol, PricePerShare, DateTimeStamp) Values (?,?,?);");
				stockHistory.setString(1, stockSymbol);
				stockHistory.setDouble(2, newPrice);
				stockHistory.setString(3, stockDateTimeStamp);
				stockHistory.executeUpdate();
				
				// update conditional orders
				// holds only unsold conditional orders
				ArrayList<String[]> arr = new ArrayList<String[]>(); 
				query = "SELECT * FROM ConditionalHistoryTable ORDER BY OrderId ASC";
				result = st.executeQuery(query);
				while (result.next()) {
					// check if the order has been sent to market, if so no need to process it anymore
					if (result.getString("Sold").equals("t")) {
						continue;
					} else {
						String[] entry = new String[]{result.getString("OrderId"), result.getString("PricePerShare"), result.getString("StopAmount"), result.getString("DateTimeStamp"), result.getString("PriceType"), result.getString("stopValue"), result.getString("Sold")};
						arr.add(entry);
					}
				}
				
				// loop through the list. 
				for (int i = 0; i < arr.size(); i++) {
					String[] current = arr.get(i); // row entry of ConditionalHistoryTable
					String[] next;
					int id = Integer.valueOf(current[0]);
					if (i < arr.size() - 1) {
						next = arr.get(i + 1);
						if (current[0].equals(next[0])) // we want to get the latest order
							continue;
					}
					// calculate new stop price
					double newStopAmount = 0.0;
					if (current[5].indexOf("%") > 0) {
						// percent based stop
						double p = Double.valueOf(current[5].substring(0, current[5].length() - 1));
						newStopAmount = newPrice * (1 - (p / 100)); 
					} else {
						// must be dollar based stop
						double p = Double.valueOf(current[5].substring(0, current[5].length() - 1));
						newStopAmount = newPrice - p;
					}
					
					// at this point must be at latest entry for given orderid
					PreparedStatement pr = conn.prepareStatement("INSERT INTO ConditionalHistoryTable(OrderId, PricePerShare, StopAmount, DateTimeStamp, PriceType, stopValue, Sold) VALUES(?,?,?,?,?,?,?);");
					if (current[4].equals("Trailing Stop")) {
						if (newPrice > oldPrice) {
							// if so the stop amount should move up in new entry
							String test = current[5];
							
							pr.setInt(1, Integer.valueOf(current[0]));
							pr.setDouble(2, newPrice);
							pr.setDouble(3, newStopAmount);
							pr.setString(4, dateTimeStamp);
							pr.setString(5, current[4]);
							pr.setString(6, current[5]);
							pr.setString(7, "f");
							pr.executeUpdate();
							
						} else {
							// else might have to sell or
							if (newPrice < Double.valueOf(current[2])) {
								// sell at the stop price
								// first update conditional history table
								pr.setInt(1, Integer.valueOf(current[0]));
								pr.setDouble(2, newPrice);
								pr.setDouble(3, Double.valueOf(current[2]));
								pr.setString(4, dateTimeStamp);
								pr.setString(5, current[4]);
								pr.setString(6, current[5]);
								pr.setString(7, "t");
								pr.executeUpdate();
								// go through all of these orders and set the sold attribute to 't'
								String soldChange = "UPDATE ConditionalHistoryTable SET Sold=" + "\"" + 't' + "\"" + " WHERE OrderId=" + String.valueOf(id);
								st.executeUpdate(soldChange);
								// place a market order with these conditions
								query = "SELECT * FROM OrderTable WHERE Id= " + String.valueOf(id);
								ResultSet temp = st.executeQuery(query);
								while (temp.next()) {
									pendingOrder.setInt(1, id);
									pendingOrder.setInt(2, temp.getInt(2));
									pendingOrder.setString(3, dateTimeStamp);
									pendingOrder.setString(4, temp.getString(4));
									pendingOrder.setString(5, temp.getString(5));
									pendingOrder.setInt(6, Integer.valueOf(temp.getString(6)));
									pendingOrder.setString(7, temp.getString(7));
									pendingOrder.setString(8, '$' + current[2]);
									pendingOrder.setString(9, temp.getString(9));
									order.setInt(1, id);
									order.setInt(2, temp.getInt(2));
									order.setString(3, dateTimeStamp);
									order.setString(4, temp.getString(4));
									order.setString(5, temp.getString(5));
									order.setInt(6, Integer.valueOf(temp.getString(6)));
									order.setString(7, temp.getString(7));
									order.setString(8, '$' + current[2]);
									order.setString(9, temp.getString(9));
								}
								pendingOrder.executeUpdate();
								order.executeUpdate();
							} else {
								// don't sell just add
								pr.setInt(1, Integer.valueOf(current[0]));
								pr.setDouble(2, newPrice);
								pr.setDouble(3, Double.valueOf(current[2]));
								pr.setString(4, dateTimeStamp);
								pr.setString(5, current[4]);
								pr.setString(6, current[5]);
								pr.setString(7, "f");
								pr.executeUpdate();
							}
						}
					} else {
						// hidden stop logic
						if (newPrice < Double.valueOf(current[2])) {
							// sell at the stop price
							// first update conditional history table
							pr.setInt(1, Integer.valueOf(current[0]));
							pr.setDouble(2, newPrice);
							pr.setDouble(3, Double.valueOf(current[2]));
							pr.setString(4, dateTimeStamp);
							pr.setString(5, current[4]);
							pr.setString(6, current[5]);
							pr.setString(7, "t");
							pr.executeUpdate();
							// go through all of these orders and set the sold attribute to 't'
							String soldChange = "UPDATE ConditionalHistoryTable SET Sold=" + "\"" + 't' + "\"" + " WHERE OrderId=" + String.valueOf(id);
							st.executeUpdate(soldChange);
							// place a market order with these conditions
							query = "SELECT * FROM OrderTable WHERE Id= " + String.valueOf(id);
							ResultSet temp = st.executeQuery(query);
							while (temp.next()) {
								pendingOrder.setInt(1, id);
								pendingOrder.setInt(2, temp.getInt(2));
								pendingOrder.setString(3, dateTimeStamp);
								pendingOrder.setString(4, temp.getString(4));
								pendingOrder.setString(5, temp.getString(5));
								pendingOrder.setInt(6, Integer.valueOf(temp.getString(6)));
								pendingOrder.setString(7, temp.getString(7));
								pendingOrder.setString(8, '$' + current[2]);
								pendingOrder.setString(9, temp.getString(9));
								order.setInt(1, id);
								order.setInt(2, temp.getInt(2));
								order.setString(3, dateTimeStamp);
								order.setString(4, temp.getString(4));
								order.setString(5, temp.getString(5));
								order.setInt(6, Integer.valueOf(temp.getString(6)));
								order.setString(7, temp.getString(7));
								order.setString(8, '$' + current[2]);
								order.setString(9, temp.getString(9));
							}
							pendingOrder.executeUpdate();
							order.executeUpdate();
						} else {
							// don't sell just add
							pr.setInt(1, Integer.valueOf(current[0]));
							pr.setDouble(2, newPrice);
							pr.setDouble(3, Double.valueOf(current[2]));
							pr.setString(4, dateTimeStamp);
							pr.setString(5, current[4]);
							pr.setString(6, current[5]);
							pr.setString(7, "f");
							pr.executeUpdate();
						}
					}
				}
				
				break;
			case "other":
				break;
			}
			
			request.getRequestDispatcher("stockManagement.jsp").forward(request, response);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

