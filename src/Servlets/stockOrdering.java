package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JDBC.Conn;

/**
 * Servlet implementation class stockOrdering
 */
@WebServlet("/stockOrdering")
public class stockOrdering extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public stockOrdering() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	
	// NEED TO UPDATE STOCK
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String hidden = (String)request.getParameter("hidden");
			String userName = (String)request.getSession().getAttribute("userName");
			Connection conn = Conn.getMySQLConnection();
			// market
			String orderType = request.getParameter("orderType");
			String quantity = request.getParameter("quantity");
			String symbol = request.getParameter("symbol");
			String priceType = request.getParameter("priceType");
				
			String query = "SELECT MAX(id) AS ID FROM OrderTable";
			Statement st = conn.createStatement();
			ResultSet result = st.executeQuery(query);
			int id = 0;
			while (result.next()) {
				id = result.getInt("ID");
			}
			
			id = id + 1; // next order 
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String dateTimeStamp = dateFormat.format(date);
			
			query = "select C.Id from clienttable C, Person P where P.userName =" + "\"" + userName + "\""
			+ " AND P.SSN = C.Id";
			result = st.executeQuery(query);
			String clientId = "";
			while(result.next()) {
				clientId = result.getString("Id");
			}
			String account = request.getParameter("account");
		
			//System.out.println(clientId);
			//TODO: Change to PreparedStatement
			if (hidden.equals("market")) {
				query = "INSERT INTO OrderTable(Id, NumShares, DateTimeStamp, ClientID, AccNum, StockSymbol, stopValue, PriceType, OrderType) VALUES "
						+ "( " + id + "," + quantity + "," + "\"" + dateTimeStamp.toString() + "\""  + "," + "\"" + clientId + "\"" + "," +  account + "," + "\"" + symbol + "\"" + "," + "null" + "," + "\"" + priceType + "\"" + "," + "\"" + orderType + "\"" + ")";
				st.executeUpdate(query);
				query = "INSERT INTO PendingOrder(Id, NumShares, DateTimeStamp, ClientID, AccNum, StockSymbol, stopValue, PriceType, OrderType) VALUES "
						+ "( " + id + "," + quantity + "," + "\"" + dateTimeStamp.toString() + "\""  + "," + "\"" + clientId + "\"" + "," +  account + "," + "\"" + symbol + "\"" + "," + "null" + "," + "\"" + priceType + "\"" + "," + "\"" + orderType + "\"" + ")";
				st.executeUpdate(query);

				} else if (hidden.equals("conditional")) {
					String stopValue = request.getParameter("stopValue");
					if (priceType.equals("TrailingStopDollar")) {
						priceType = "Trailing Stop";
						stopValue = stopValue + "$";
					} else if (priceType.equals("TrailingStopPercent")) {
						priceType = "Trailing Stop";
						stopValue = stopValue + "%";
					} else {
						priceType = "Hidden Stop";
						stopValue = stopValue + "$";
					}
					
					query = "Select S.PricePerShare FROM StockTable S WHERE S.StockSymbol = " + "\"" + symbol + "\"";
					result = st.executeQuery(query);
					double pricePerShare = 0.0;
					while (result.next()) {
						pricePerShare = result.getDouble("PricePerShare");
					}
					
					double stopAmount = 0.0;
					if (stopValue.indexOf("%") > 0) {
						double p = Double.valueOf(stopValue.substring(0, stopValue.length() - 1));
						stopAmount = pricePerShare * (1 - (p / 100));
					} else {
						double p = Double.valueOf(stopValue.substring(0, stopValue.length() - 1));
						stopAmount = pricePerShare - p;
					}
					 
					query = "INSERT INTO OrderTable(Id, NumShares, DateTimeStamp, ClientID, AccNum, StockSymbol, stopValue, PriceType, OrderType) VALUES "
							+ "( " + id + "," + quantity + "," + "\"" + dateTimeStamp.toString() + "\""  + "," + "\"" + clientId + "\"" + "," +  account + "," + "\"" + symbol + "\"" + "," + "\"" + stopValue + "\"" + "," + "\"" + priceType + "\"" + "," + "\"" + orderType + "\"" + ")";
					st.executeUpdate(query);
					
					PreparedStatement pr = conn.prepareStatement("INSERT INTO ConditionalHistoryTable(OrderId, PricePerShare, StopAmount, DateTimeStamp, PriceType, stopValue, Sold) VALUES(?,?,?,?,?,?,?);");
					pr.setInt(1, id);
					pr.setDouble(2, pricePerShare);
					pr.setDouble(3, stopAmount);
					pr.setString(4, dateTimeStamp);
					pr.setString(5, priceType);
					pr.setString(6, stopValue);
					pr.setString(7, "f");
					pr.executeUpdate();
				}
			request.getRequestDispatcher("/customer/customerPage.jsp").forward(request, response);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doGet(request, response);
	}

}
