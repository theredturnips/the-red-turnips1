package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBC.Conn;
import beans.Stock;

/**
 * Servlet implementation class customerActions
 */
@WebServlet("/customerActions")
public class customerActions extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public customerActions() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = (String)request.getParameter("hidden");
		String clientView = "";
		try {
			Connection conn = (Connection) Conn.getMySQLConnection();
			HttpSession session = request.getSession();
			String userName = (String)session.getAttribute("userName");
			String pass = (String)session.getAttribute("password");
			String query = "";
			Statement st = conn.createStatement();
			ResultSet result;
			if(action.equals("bestSellerList")){	
				query = "SELECT O.StockSymbol, Count(O.StockSymbol) AS amt FROM OrderTable O GROUP BY O.StockSymbol ORDER BY amt DESC LIMIT 5";
				result = st.executeQuery(query);
				ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
				String stock = "";
				String amt = "";
				while(result.next()){
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("StockSymbol", result.getString("StockSymbol"));
					map.put("Amount", String.valueOf(result.getInt("Amt")));
					list.add(map);
				}
				session.setAttribute("bestsellerlist", list);
				session.setAttribute("switch", "Best Seller List");
				request.getRequestDispatcher("customer/clientView.jsp").forward(request, response);
					
			} else if(action.equals("trailingHistory")) {
				int id = Integer.valueOf(request.getParameter("orderId"));
				query = "SELECT * FROM ConditionalHistoryTable WHERE OrderId= " + id + " ORDER BY DateTimeStamp";
				result = st.executeQuery(query);
				ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
				String dates = "";
				String stopPrices = "";
				String prices = "";
				while (result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("OrderId", String.valueOf(result.getInt(1)));
					map.put("PricePerShare", String.valueOf(result.getDouble(2)));
					map.put("stopAmount", String.valueOf(result.getDouble(3)));
					map.put("DateTimeStamp", result.getString(4));
					prices += String.valueOf(result.getDouble(2)) + ",";
					stopPrices += String.valueOf(result.getDouble(3) + ",");
					dates += result.getString(4) + ",";
					list.add(map);
				}

				session.setAttribute("trailingPrices", prices);
				session.setAttribute("trailingStopPrices", stopPrices);
				session.setAttribute("trailingDates", dates);
				session.setAttribute("trailingHistory", list);
				session.setAttribute("switch", "Trailing History");
				request.getRequestDispatcher("customer/clientView.jsp").forward(request, response);
			} else if(action.equals("hiddenHistory")) {
				int id = Integer.valueOf(request.getParameter("orderId"));
				query = "SELECT * FROM ConditionalHistoryTable WHERE OrderId= " + id + " ORDER BY DateTimeStamp";
				result = st.executeQuery(query);
				ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
				String dates = "";
				String prices = "";
				String stopPrices = "";
				while (result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("OrderId", String.valueOf(result.getInt(1)));
					map.put("PricePerShare", String.valueOf(result.getDouble(2)));
					map.put("stopAmount", String.valueOf(result.getDouble(3)));
					map.put("DateTimeStamp", result.getString(4));
					prices += String.valueOf(result.getDouble(2)) + ",";
					stopPrices += String.valueOf(result.getDouble(3) + ",");
					dates += result.getString(4) + ",";
					list.add(map);
				}

				session.setAttribute("hiddenPrices", prices);
				session.setAttribute("hiddenStopPrices", stopPrices);
				session.setAttribute("hiddenDates", dates);
				session.setAttribute("hiddenHistory", list);
				session.setAttribute("switch", "Hidden History");
				request.getRequestDispatcher("customer/clientView.jsp").forward(request, response);
			} else if (action.equals("suggestions")) {
				query = "SELECT * FROM StockSuggestion WHERE ClientId=(SELECT SSN FROM Person WHERE username=" + "\"" + userName + "\"" + ")";
				result = st.executeQuery(query);

				ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
				while (result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("StockSymbol", result.getString("StockSymbol"));
					map.put("CompanyName", result.getString("CompanyName"));
					map.put("StockType", result.getString("StockType"));
					map.put("PricePerShare", String.valueOf(result.getDouble("PricePerShare")));
					map.put("NumShares", String.valueOf(result.getInt("NumShares")));
					list.add(map);
				}

				session.setAttribute("suggestions", list);
				session.setAttribute("switch", "Stock Suggestions");
				request.getRequestDispatcher("customer/clientView.jsp").forward(request, response);

			} else if (action.equals("stockHistory")) {
				String symbol = request.getParameter("symbol");
				int numMonths = Integer.valueOf(request.getParameter("numMonths"));
				query = "SELECT * FROM StockHistory WHERE StockSymbol=" + "\"" + symbol + "\""  + " ORDER BY DateTimeStamp";
				result = st.executeQuery(query);

				// get current data
				SimpleDateFormat stockDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				Date current = new Date();

				Calendar c = Calendar.getInstance(); 
				c.setTime(current); 
				c.add(Calendar.MONTH, -1 * numMonths);
				current = c.getTime();

				ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
				String prices = "";
				String dates = "";

				while (result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					String date = result.getString("DateTimeStamp");
					Date temp = stockDateFormat.parse(date);
					if (temp.after(current)) {
						map.put("StockSymbol", result.getString("StockSymbol"));
						map.put("PricePerShare", String.valueOf(result.getDouble("PricePerShare")));
						map.put("DateTimeStamp", result.getString("DateTimeStamp"));
						prices += String.valueOf(result.getDouble("PricePerShare")) + ",";
						dates += result.getString("DateTimeStamp") + ",";
						list.add(map);
					}
				}

				// add the one data point
				if (list.isEmpty()) {
					query = "SELECT * FROM StockHistory WHERE StockSymbol=" + "\"" + symbol + "\"";
					result = st.executeQuery(query);
					while (result.next()) {
						HashMap<String, String> map = new HashMap<String, String>();
						String date = result.getString("DateTimeStamp");
						Date temp = stockDateFormat.parse(date);
						map.put("StockSymbol", result.getString("StockSymbol"));
						map.put("PricePerShare", String.valueOf(result.getDouble("PricePerShare")));
						map.put("DateTimeStamp", result.getString("DateTimeStamp"));
						prices += String.valueOf(result.getDouble("PricePerShare")) + ",";
						dates += result.getString("DateTimeStamp") + ",";
						list.add(map);
					}
				}

				session.setAttribute("stockPrices", prices);
				session.setAttribute("stockDates", dates);
				session.setAttribute("stockHistory", list);
				session.setAttribute("switch", "Stock History");
				request.getRequestDispatcher("customer/clientView.jsp").forward(request, response);
			} else if(action.equals("allOrders")) {
				query = "SELECT * FROM OrderTable WHERE ClientId=(SELECT SSN FROM Person WHERE username=" + "\"" + userName + "\"" + ")";
				ArrayList<HashMap<String, String>> orderList = new ArrayList<HashMap<String, String>>();
				result = st.executeQuery(query);
				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("orderId", String.valueOf(result.getString("Id")));
					map.put("numShares", String.valueOf(result.getInt("NumShares")));
					map.put("dateTimeStamp", result.getString("DateTimeStamp"));
					map.put("clientID", result.getString("ClientID"));
					map.put("accNum", result.getString("AccNum"));
					map.put("stockSymbol", result.getString("StockSymbol"));
					map.put("priceType", result.getString("PriceType"));
					map.put("orderType", result.getString("OrderType"));
					orderList.add(map);
				}
				session.setAttribute("orderHistory", orderList);
				session.setAttribute("switch", "Order History");
				request.getRequestDispatcher("customer/clientView.jsp").forward(request, response);
			} else if(action.equals("stockType")) {
				String type = request.getParameter("stockTypeList");
				query = "SELECT * From StockTable WHERE StockType = " + "\"" + type + "\"";
				ArrayList<HashMap<String, String>> stockList = new ArrayList<HashMap<String, String>>();
				result = st.executeQuery(query);
				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("StockSymbol", result.getString("StockSymbol"));
					map.put("CompanyName", result.getString("CompanyName"));
					map.put("StockType", result.getString("StockType"));
					map.put("PricePerShare", String.valueOf(result.getDouble("PricePerShare")));
					map.put("NumShares", String.valueOf(result.getInt("NumShares")));
					stockList.add(map);
				}
				session.setAttribute("stockTypes", stockList);
				session.setAttribute("switch", "Stocks By Type");
				request.getRequestDispatcher("customer/clientView.jsp").forward(request, response);
			} else if (action.equals("keyword")) {
				String key = request.getParameter("search");
				String[] keys = key.split(" ");
				String suffix = "";
				for (int i = 0; i < keys.length; i++) {
					suffix += "CompanyName LIKE " + "\"" + "%" + keys[i] + "%" + "\"" + " OR ";
				}
				suffix = suffix.substring(0, suffix.lastIndexOf("OR"));
				query = "SELECT * FROM StockTable WHERE " + suffix;
				ArrayList<HashMap<String, String>> stockList = new ArrayList<HashMap<String, String>>();
				result = st.executeQuery(query);
				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("StockSymbol", result.getString("StockSymbol"));
					map.put("CompanyName", result.getString("CompanyName"));
					map.put("StockType", result.getString("StockType"));
					map.put("PricePerShare", String.valueOf(result.getDouble("PricePerShare")));
					map.put("NumShares", String.valueOf(result.getInt("NumShares")));
					stockList.add(map);
				}
				session.setAttribute("stockSearches", stockList);
				session.setAttribute("switch", "Stocks By Keyword");
				request.getRequestDispatcher("customer/clientView.jsp").forward(request, response);
			} else if (action.equals("newAccount")) {
				query = "SELECT Id From ClientTable WHERE Id=(SELECT SSN FROM Person WHERE username=" + "\"" + userName + "\"" + ")";
				String clientId = "";
				result = st.executeQuery(query);
				while (result.next()) {
					clientId = result.getString("Id");
				}
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				Date current = new Date();
				String date = dateFormat.format(current);
				
				int newAccNum = 0;
				query = "SELECT MAX(id) AS ID FROM AccountTable WHERE clientId =" + "\"" + clientId + "\"";
				result = st.executeQuery(query);
				while (result.next()) {
					newAccNum = result.getInt("ID");
				}
				newAccNum++;
				
				query = "INSERT INTO AccountTable(Id, DateOpened, ClientId) VALUES (?,?,?);";
				PreparedStatement pr = conn.prepareStatement(query);
				pr.setInt(1, newAccNum);
				pr.setString(2, date);
				pr.setString(3, clientId);
				pr.executeUpdate();
				
				request.getRequestDispatcher("customer/customerPage.jsp").forward(request, response);
			} else if (action.equals("receipts")){
				query = "SELECT * FROM Receipt WHERE ClientId=(SELECT SSN FROM Person WHERE username=" + "\"" + userName + "\"" + ")";
				result = st.executeQuery(query);
				HashMap<String, String> map = new HashMap<String, String>();
				ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
				while(result.next()){
					map.put("DateTimeStamp", result.getString("DateTimeStamp"));
					map.put("StockSymbol", result.getString("StockSymbol"));
					map.put("NumShares", String.valueOf(result.getInt("NumShares")));
					map.put("ClientID", String.valueOf(result.getInt("ClientID")));
					map.put("OrderType", result.getString("OrderType"));
					list.add(map);
				}
				session.setAttribute("Receipts", list);
				session.setAttribute("switch", "Receipts");
				request.getRequestDispatcher("customer/clientView.jsp").forward(request, response);
			}
		} catch (Exception e) {
			request.getRequestDispatcher("errorPage.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
