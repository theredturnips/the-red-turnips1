package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBC.Conn;
import beans.Stock;
import utilities.DatabaseBackup;

/**
 * Servlet implementation class managerActions
 */
@WebServlet("/managerActions")
public class managerActions extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public managerActions() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = (String)request.getParameter("action");
		System.out.println(action);

		try {
			Connection conn = (Connection) Conn.getMySQLConnection();
			HttpSession session = request.getSession();
			ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
			if(action.equals("stockManagement")){
				request.getRequestDispatcher("stockManagement.jsp").forward(request, response);
			}
			else if (action.equals("employeeManagement")) {
				request.getRequestDispatcher("employee/employeeManagement.jsp").forward(request, response);
			}
			else if (action.equals("refreshEmployeeList")) {
				String query = "SELECT * FROM EmployeeTable join Person on EmployeeTable.SSN=Person.SSN";
				PreparedStatement pr = conn.prepareStatement(query);
				ResultSet result = pr.executeQuery();
				
				ArrayList<HashMap<String, String>> employeeList = new ArrayList<HashMap<String, String>>();
				Statement st = conn.createStatement();
				result = st.executeQuery("SELECT * FROM EmployeeTable join Person on EmployeeTable.SSN=Person.SSN");

				while(result.next()){
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Id", String.valueOf(result.getString("Id")));
					map.put("LastName", String.valueOf(result.getString("LastName")));
					map.put("FirstName", String.valueOf(result.getString("FirstName")));
					map.put("SSN", String.valueOf(result.getString("SSN")));
					map.put("Address", String.valueOf(result.getString("Address")));
					map.put("Zipcode", String.valueOf(result.getInt("Zipcode")));
					map.put("Telephone",  String.valueOf(result.getInt("Telephone")));
					map.put("StartDate", result.getString("StartDate"));
					map.put("HourlyRate", result.getString("HourlyRate"));
					employeeList.add(map);
				}

				session.setAttribute("employeeList", employeeList);	
				request.getRequestDispatcher("employee/employeeManagement.jsp").forward(request, response);
			}
			else if(action.equals("backupDatabase")){
				DatabaseBackup dbBackup = new DatabaseBackup();
				dbBackup.dbBackup();
				request.getRequestDispatcher("/stockManagement.jsp").forward(request, response);
			}
			else if (action.equals("addEmployee")) {
				request.getRequestDispatcher("addEmployee.jsp").forward(request, response);
			}
			else if (action.equals("addStockPage")) {
				request.getRequestDispatcher("addStock.jsp").forward(request, response);
			}
			
			else if (action.equals("editEmployee")) {
				//String employeeId = (String)request.getParameter("selectedEmployee");
				String selectedEmployeeInfo = (String)request.getParameter("selectedEmployeeInfo");
				System.out.println(selectedEmployeeInfo);

				// store data in dictionary and send it to the jsp page for editing
				HashMap<String, String> employeeInfo = new HashMap<String, String>();

				int index = selectedEmployeeInfo.indexOf(",");
				String id = selectedEmployeeInfo.substring(0, index);
				selectedEmployeeInfo = selectedEmployeeInfo.substring(index + 1);

				index = selectedEmployeeInfo.indexOf(",");
				String lastName = selectedEmployeeInfo.substring(0, index);
				selectedEmployeeInfo = selectedEmployeeInfo.substring(index + 1);

				index = selectedEmployeeInfo.indexOf(",");
				String firstName = selectedEmployeeInfo.substring(0, index);
				selectedEmployeeInfo = selectedEmployeeInfo.substring(index + 1);

				index = selectedEmployeeInfo.indexOf(",");
				String SSN = selectedEmployeeInfo.substring(0, index);
				selectedEmployeeInfo = selectedEmployeeInfo.substring(index + 1);

				index = selectedEmployeeInfo.indexOf(",");
				String address = selectedEmployeeInfo.substring(0, index);
				selectedEmployeeInfo = selectedEmployeeInfo.substring(index + 1);

				index = selectedEmployeeInfo.indexOf(",");
				String ZipCode = selectedEmployeeInfo.substring(0, index);
				selectedEmployeeInfo = selectedEmployeeInfo.substring(index + 1);

				index = selectedEmployeeInfo.indexOf(",");
				String telephone = selectedEmployeeInfo.substring(0, index);
				selectedEmployeeInfo = selectedEmployeeInfo.substring(index + 1);

				index = selectedEmployeeInfo.indexOf(",");
				String StartDate = selectedEmployeeInfo.substring(0, index);
				selectedEmployeeInfo = selectedEmployeeInfo.substring(index + 1);

				index = selectedEmployeeInfo.indexOf(",");
				String HourlyRate = selectedEmployeeInfo.substring(0, index);
				selectedEmployeeInfo = selectedEmployeeInfo.substring(index + 1);

				employeeInfo.put("Id", id);
				employeeInfo.put("lastName", lastName);
				employeeInfo.put("firstName", firstName);
				employeeInfo.put("SSN", SSN);
				employeeInfo.put("address", address);
				employeeInfo.put("ZipCode", ZipCode);
				employeeInfo.put("telephone", telephone);
				employeeInfo.put("StartDate", StartDate);
				employeeInfo.put("HourlyRate", HourlyRate);

				session.setAttribute("emoloyeeInfo", employeeInfo);
				request.getRequestDispatcher("editEmployee.jsp").forward(request, response);
			}	
			else if (action.equals("refreshCustomerList")) {
				String query = "SELECT * FROM ClientTable join Person on ClientTable.Id=Person.SSN and ClientTable.active=true";
				PreparedStatement pr = conn.prepareStatement(query);
				ResultSet result = pr.executeQuery();
				
				ArrayList<HashMap<String, String>> customerList = new ArrayList<HashMap<String, String>>();
				Statement st = conn.createStatement();
				result = st.executeQuery("SELECT * FROM ClientTable join Person on ClientTable.Id=Person.SSN and ClientTable.active=true");

				while(result.next()){
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Id", String.valueOf(result.getString("Id")));
					map.put("LastName", String.valueOf(result.getString("LastName")));
					map.put("FirstName", String.valueOf(result.getString("FirstName")));
					map.put("SSN", String.valueOf(result.getString("SSN")));
					map.put("Address", String.valueOf(result.getString("Address")));
					map.put("Zipcode", String.valueOf(result.getInt("Zipcode")));
					map.put("Telephone",  String.valueOf(result.getInt("Telephone")));
					map.put("Email", result.getString("Email"));
					map.put("CreditCardNumber", result.getString("CreditCardNumber"));
					map.put("Rating", result.getString("Rating"));
					customerList.add(map);
				}

				session.setAttribute("customerList", customerList);	
				request.getRequestDispatcher("employee/employeeManagement.jsp").forward(request, response);
			}
			else if (action.equals("addCustomer")) {
				request.getRequestDispatcher("addCustomer.jsp").forward(request, response);
			}
			else if (action.equals("deleteCustomer")) {
				// delete an employee
				String id = (String)request.getParameter("selectedCustomer");
				System.out.println(id);
				
				//DELETE FROM EmployeeTable WHERE EmployeeTable.SSN = "123-45-6789";
				String query = "UPDATE ClientTable SET active = false WHERE Id= ?";
				PreparedStatement update = conn.prepareStatement(query);
				update = conn.prepareStatement(query);
				update.setString(1, id);
				update.executeUpdate();
				
				request.getRequestDispatcher("employee/employeeManagement.jsp").forward(request, response);	
			}
			else if (action.equals("refreshStockList")) {
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery("SELECT * FROM StockTable");
				ArrayList<HashMap<String, String>> stockList = new ArrayList<HashMap<String, String>>();
				
				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("StockSymbol", result.getString("StockSymbol"));
					map.put("CompanyName", result.getString("CompanyName"));
					map.put("StockType", result.getString("StockType"));
					map.put("PricePerShare", String.valueOf(Double.toString(result.getDouble("PricePerShare"))));
					map.put("NumShares", String.valueOf(result.getInt("NumShares")));
					stockList.add(map);
				}

				session.setAttribute("stock_List", stockList);
				request.getRequestDispatcher("stockManagement.jsp").forward(request, response);
			} 
			else if (action.equals("sortStockList")) {
//				select S.*
//				From OrderTable O, StockTable S
//				Where O.StockSymbol=S.StockSymbol
//				Group by O.StockSymbol
//				Order By Count(O.StockSymbol) DESC
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery("SELECT S.* FROM OrderTable O, StockTable S "
						+ "Where O.StockSymbol=S.StockSymbol "
						+ "Group by O.StockSymbol "
						+ "Order By Count(O.StockSymbol) DESC");
				ArrayList<HashMap<String, String>> stockList = new ArrayList<HashMap<String, String>>();
				
				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("StockSymbol", result.getString("StockSymbol"));
					map.put("CompanyName", result.getString("CompanyName"));
					map.put("StockType", result.getString("StockType"));
					map.put("PricePerShare", String.valueOf(Double.toString(result.getDouble("PricePerShare"))));
					map.put("NumShares", String.valueOf(result.getInt("NumShares")));
					stockList.add(map);
				}

				session.setAttribute("stock_List", stockList);
				request.getRequestDispatcher("stockManagement.jsp").forward(request, response);
			} 
			else if (action.equals("refreshCustomerpage")) {
				String query = "SELECT * FROM ClientTable WHERE active=true";
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery(query);
				ArrayList<HashMap<String, String>> clientList = new ArrayList<HashMap<String, String>>();

				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Id", result.getString("Id"));
					map.put("CreditCardNumber", result.getString("CreditCardNumber"));
					map.put("Email", result.getString("Email"));
					map.put("rating", String.valueOf(Double.toString(result.getDouble("rating"))));
					clientList.add(map);
				}

				session.setAttribute("clientList", clientList);
				request.getRequestDispatcher("stockManagement.jsp").forward(request, response);
			}
			else if (action.equals("sortCustomerList")) {
//				select S.*
//				From OrderTable O, StockTable S
//				Where O.StockSymbol=S.StockSymbol
//				Group by O.StockSymbol
//				Order By Count(O.StockSymbol) DESC
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery("SELECT S.* FROM OrderTable O, StockTable S "
						+ "Where O.StockSymbol=S.StockSymbol "
						+ "Group by O.StockSymbol "
						+ "Order By Count(O.StockSymbol) DESC");
				ArrayList<HashMap<String, String>> customerList = new ArrayList<HashMap<String, String>>();
				
				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Id", result.getString("Id"));
					map.put("CreditCardNumber", result.getString("CreditCardNumber"));
					map.put("Email", result.getString("Email"));
					map.put("rating", String.valueOf(Double.toString(result.getDouble("rating"))));
					customerList.add(map);
				}

				session.setAttribute("customerList", customerList);
				request.getRequestDispatcher("stockManagement.jsp").forward(request, response);
			} 
			else if (action.equals("refreshStockTypeList")) {
				String query = "SELECT DISTINCT StockType FROM StockTable";
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery(query);
				ArrayList<Hashtable<String, String>> stockTypeList = new ArrayList<Hashtable<String, String>>();

				while(result.next()) {
					Hashtable<String, String> map = new Hashtable<String, String>();
					map.put("StockType", result.getString("StockType"));
					stockTypeList.add(map);
				}

				session.setAttribute("stockTypeList", stockTypeList);
				request.getRequestDispatcher("stockManagement.jsp").forward(request, response);
			}
			else if (action.equals("showStockOrderHistory")) {
				String selectedStock = request.getParameter("selectedStock");
				String query = "SELECT * FROM OrderTable WHERE OrderTable.StockSymbol = " + "\"" + selectedStock + "\"";
				System.out.println(query);
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery(query);
				ArrayList<HashMap<String, String>> orderHistory = new ArrayList<HashMap<String, String>>();

				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Id", result.getString("Id"));
					map.put("ClientID", result.getString("ClientID"));
					map.put("StockSymbol", result.getString("StockSymbol"));
					map.put("NumShares", result.getString("NumShares"));
					map.put("AccNum", result.getString("AccNum"));
					map.put("OrderType", result.getString("OrderType"));
					map.put("PriceType", result.getString("PriceType"));
					map.put("stopValue", result.getString("stopValue"));
					map.put("DateTimeStamp", result.getString("DateTimeStamp"));
					orderHistory.add(map);
				}
				session.setAttribute("orderHistory", orderHistory);
				request.getRequestDispatcher("orderHistory.jsp").forward(request, response);
			}
			else if (action.equals("showStockRevenue")){
				String selectedStock = request.getParameter("selectedStock");
				String query = "SELECT SUM(Tr.Fee) as Revenue FROM TradeTable T, TransactionTable Tr WHERE T.TransactionId = Tr.Id AND T.StockId = ?";
				PreparedStatement pr = conn.prepareStatement(query);
				pr.setString(1, selectedStock);
				ResultSet result = pr.executeQuery();
				
				ArrayList<HashMap<String, String>> stockRevenue = new ArrayList<HashMap<String, String>>();
				
				while(result.next()){
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Name", selectedStock);
					map.put("Revenue", String.valueOf(result.getDouble("Revenue")));
					stockRevenue.add(map);
				}
				session.setAttribute("revenueDisplayList", stockRevenue);
				request.getRequestDispatcher("revenueSummary.jsp").forward(request, response);
			}
			else if (action.equals("showCustomerRevenue")){
				String selectedCustomer = request.getParameter("selectedCustomer");
				//SELECT SUM(Tr.Fee) as Revenue FROM TradeTable T, TransactionTable Tr, AccountTable A WHERE T.TransactionId = Tr.Id AND
				//T.ClientId = A.ClientId AND A.ClientId = "222-22-2222"
				String query = "SELECT SUM(Tr.Fee) as Revenue FROM TradeTable T, TransactionTable Tr, AccountTable A WHERE T.TransactionId = Tr.Id AND T.ClientId = A.ClientId AND A.ClientId =?";
				PreparedStatement pr = conn.prepareStatement(query);
				pr.setString(1, selectedCustomer);
				ResultSet result = pr.executeQuery();
				
				ArrayList<HashMap<String, String>> customerRevenue = new ArrayList<HashMap<String, String>>();
				
				while(result.next()){
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Name", selectedCustomer);
					map.put("Revenue", String.valueOf(result.getDouble("Revenue")));
					customerRevenue.add(map);
				}
				session.setAttribute("revenueDisplayList", customerRevenue);
				request.getRequestDispatcher("revenueSummary.jsp").forward(request, response);
			}
			else if (action.equals("showStockTypeRevenue")){
				String selectedStockType = request.getParameter("selectedStockType");
				String query = "SELECT SUM(Tr.Fee) as Revenue FROM TradeTable T, TransactionTable Tr, StockTable S WHERE T.TransactionId = Tr.Id AND T.StockId = S.StockSymbol AND S.StockType =?";
				PreparedStatement pr = conn.prepareStatement(query);
				pr.setString(1, selectedStockType);
				ResultSet result = pr.executeQuery();
				
				ArrayList<HashMap<String, String>> stockTypeRevenue = new ArrayList<HashMap<String, String>>();
				
				while(result.next()){
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Name", selectedStockType);
					map.put("Revenue", String.valueOf(result.getDouble("Revenue")));
					stockTypeRevenue.add(map);
				}
				session.setAttribute("revenueDisplayList", stockTypeRevenue);
				request.getRequestDispatcher("revenueSummary.jsp").forward(request, response);
			}
			else if (action.equals("showCustomerOrderHistory")) {
				String selectedCustomer = request.getParameter("selectedCustomer");
				String query = "SELECT * FROM OrderTable WHERE OrderTable.ClientID = " + "\"" + selectedCustomer + "\"";
				System.out.println(query);
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery(query);
				ArrayList<HashMap<String, String>> orderHistory = new ArrayList<HashMap<String, String>>();

				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Id", result.getString("Id"));
					map.put("ClientID", result.getString("ClientID"));
					map.put("StockSymbol", result.getString("StockSymbol"));
					map.put("NumShares", result.getString("NumShares"));
					map.put("AccNum", result.getString("AccNum"));
					map.put("OrderType", result.getString("OrderType"));
					map.put("PriceType", result.getString("PriceType"));
					map.put("stopValue", result.getString("stopValue"));
					map.put("DateTimeStamp", result.getString("DateTimeStamp"));
					orderHistory.add(map);
				}
				session.setAttribute("orderHistory", orderHistory);
				request.getRequestDispatcher("orderHistory.jsp").forward(request, response);
			}
			else if (action.equals("refreshBestEmployee")) {
//				select EmployeeTable.id, sum(TransactionTable.Fee)
//				from EmployeeTable, TradeTable, TransactionTable
//				where EmployeeTable.Id=TradeTable.BrokerId and TradeTable.TransactionId=TransactionTable.Id
//				group by EmployeeTable.id
//				order by sum(TransactionTable.Fee) DESC LIMIT 1

				String query = "SELECT EmployeeTable.Id, sum(TransactionTable.Fee) "
						+ "FROM  EmployeeTable, TradeTable, TransactionTable "
						+ "WHERE EmployeeTable.Id=TradeTable.BrokerId and TradeTable.TransactionId=TransactionTable.Id "
						+ "group by EmployeeTable.Id "
						+ "order by sum(TransactionTable.Fee) DESC LIMIT 1";
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery(query);
				ArrayList<HashMap<String, String>> bestEmployee = new ArrayList<HashMap<String, String>>();
				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Id", result.getString("Id"));
					map.put("ClientID", result.getString("sum(TransactionTable.Fee)"));
					bestEmployee.add(map);
				}
				int bestID = Integer.valueOf(bestEmployee.get(0).get("Id"));
				
				String query2 = "SELECT Person.FirstName, Person.LastName FROM Person, EmployeeTable WHERE Person.SSN=EmployeeTable.SSN and EmployeeTable.Id=" + bestID;
				ResultSet result2 = st.executeQuery(query2);
				ArrayList<HashMap<String, String>> bestEmployeeName = new ArrayList<HashMap<String, String>>();
				while(result2.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("LastName", String.valueOf(result2.getString("LastName")));
					map.put("FirstName", String.valueOf(result2.getString("FirstName")));
					bestEmployeeName.add(map);
				}
				session.setAttribute("bestEmployeeName", bestEmployeeName);
				request.getRequestDispatcher("stockManagement.jsp").forward(request, response);
				
			}
			else if (action.equals("refreshBestCustomer")) {
//				select EmployeeTable.id, sum(TransactionTable.Fee)
//				from EmployeeTable, TradeTable, TransactionTable
//				where EmployeeTable.Id=TradeTable.BrokerId and TradeTable.TransactionId=TransactionTable.Id
//				group by EmployeeTable.id
//				order by sum(TransactionTable.Fee) DESC LIMIT 1
				//String query = "SELECT SUM(Tr.Fee) as Revenue, A.ClientId FROM TradeTable T, TransactionTable Tr, AccountTable A WHERE T.TransactionId = Tr.Id AND T.ClientId = A.ClientId GROUP BY A.ClientId Order By Revenue DESC";
				
				String query = "SELECT SUM(Tr.Fee) as Revenue, A.ClientId FROM TradeTable T, "
						+ "TransactionTable Tr, AccountTable A WHERE T.TransactionId = Tr.Id "
						+ "AND T.ClientId = A.ClientId GROUP BY A.ClientId Order By Revenue DESC";
				
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery(query);
				ArrayList<HashMap<String, String>> bestCustomer = new ArrayList<HashMap<String, String>>();
				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("ClientId", result.getString("ClientId"));
					map.put("Revenue", result.getString("Revenue"));
					bestCustomer.add(map);
				}
				
				String ClientID = bestCustomer.get(0).get("ClientId");
				//select Person.FirstName, Person.LastName FROM Person, where ClientTable.ClientId = Person.SSN
				
				String query2 = "select Person.FirstName, Person.LastName FROM Person where Person.SSN = " + "\"" + ClientID + "\"";
				//String query2 = "SELECT Person.FirstName, Person.LastName FROM Person EmployeeTable WHERE Person.SSN=EmployeeTable.SSN and EmployeeTable.Id=" + bestID;
				ResultSet result2 = st.executeQuery(query2);
				ArrayList<HashMap<String, String>> bestCustomerName = new ArrayList<HashMap<String, String>>();
				while(result2.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("LastName", String.valueOf(result2.getString("LastName")));
					map.put("FirstName", String.valueOf(result2.getString("FirstName")));
					bestCustomerName.add(map);
				}
				session.setAttribute("bestCustomerName", bestCustomerName);
				request.getRequestDispatcher("stockManagement.jsp").forward(request, response);
				
			}
			else if (action.equals("customerRevenueSummary")) {
				String query = "SELECT SUM(Tr.Fee) as Revenue, P.FirstName, P.LastName FROM Person P, TradeTable T, TransactionTable Tr, AccountTable A WHERE T.TransactionId = Tr.Id AND T.ClientId = A.ClientId AND A.ClientId = P.SSN GROUP BY A.ClientId Order By Revenue DESC;";
				
				String names = "";
				String revenue = "";
				
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery(query);
				while (result.next()) {
					names += result.getString("FirstName") + " " + result.getString("LastName") + ",";
					revenue += String.valueOf(result.getDouble("Revenue")) + ",";
				}
				
				session.setAttribute("customers", names);
				session.setAttribute("revenues", revenue);
				
				request.getRequestDispatcher("customerRevenue.jsp").forward(request, response);
				
			}
			
			else if (action.equals("getSaleReport")){
				int selectedYear = Integer.valueOf(request.getParameter("selectedYear"));
				int selectedMonth = Integer.valueOf(request.getParameter("selectedMonth"));
				
				String query = "SELECT * FROM OrderTable";
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery(query);
				result = st.executeQuery(query);

				// get current data
				SimpleDateFormat stockDateFormat = new SimpleDateFormat("MM-yyyy");
				
				ArrayList<HashMap<String, String>> monthList = new ArrayList<HashMap<String, String>>();
				int itemMonth = 0;
				int itemYear = 0;

				while (result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					String date2 = result.getString("DateTimeStamp").substring(0, result.getString("DateTimeStamp").indexOf(" "));
					itemYear = Integer.valueOf(date2.substring(0, date2.indexOf("-")));
					date2 = date2.substring(date2.indexOf("-") + 1);
					itemMonth = Integer.valueOf(date2.substring(0, date2.indexOf("-")));
					
					
					if (itemYear == selectedYear && itemMonth == selectedMonth) {
						map.put("Id", result.getString("Id"));
						map.put("ClientID", result.getString("ClientID"));
						map.put("StockSymbol", result.getString("StockSymbol"));
						map.put("NumShares", result.getString("NumShares"));
						map.put("AccNum", result.getString("AccNum"));
						map.put("OrderType", result.getString("OrderType"));
						map.put("PriceType", result.getString("PriceType"));
						map.put("stopValue", result.getString("stopValue"));
						map.put("DateTimeStamp", result.getString("DateTimeStamp"));
						monthList.add(map);
					}
				}
				
				if (monthList.size() == 0){
					
				}
				else{
					session.setAttribute("orderHistory", monthList);
					request.getRequestDispatcher("orderHistory.jsp").forward(request, response);
				}
			}
			else if (action.equals("getRevenueSummary")) {
				request.getRequestDispatcher("revenueSummary.jsp").forward(request, response);
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = (String)request.getParameter("action");
		System.out.println(action);

		try {
			Connection conn = (Connection) Conn.getMySQLConnection();

			if (action.equals("updateEmployeeInfo")) {
				// update employee information
				//int id = (int)session.getAttribute("Id");
				int id = Integer.valueOf(request.getParameter("Id"));
				String lastName = (String)request.getParameter("lastName");
				String firstName = (String)request.getParameter("firstName");
				String SSN = (String)request.getParameter("SSN");
				String address = (String)request.getParameter("address");
				int ZipCide = Integer.valueOf(request.getParameter("ZipCode"));
				String telephone = (String)request.getParameter("telephone");
				String StartDate = (String)request.getParameter("StartDate");
				double HourlyRate = Double.valueOf(request.getParameter("HourlyRate"));
				//	UPDATE EmployeeTable, Person 
				//	SET EmployeeTable.Id = 111, Person.SSN= "111-11-1112", LastName="Smith", FirstName="d", Address="anywhere", ZipCode=11790, Telephone="631", StartDate='2005-01-11', HourlyRate=68 
				//	WHERE Person.SSN="123-45-6789" AND EmployeeTable.SSN=Person.SSN;
				String query = "UPDATE EmployeeTable, Person "
						+ "SET EmployeeTable.Id = ?, LastName = ?, FirstName =?, Address = ?, ZipCode = ?, Telephone = ?, StartDate = ?, HourlyRate = ? "
						+ "WHERE EmployeeTable.SSN = ? AND EmployeeTable.SSN = Person.SSN";
				PreparedStatement update = conn.prepareStatement(query);
				update = conn.prepareStatement(query);
				update.setInt(1, id);
				update.setString(2, lastName);
				update.setString(3, firstName);
				update.setString(4, address);
				update.setInt(5, ZipCide);
				update.setString(6, telephone);
				update.setString(7, StartDate);
				update.setDouble(8, HourlyRate);
				update.setString(9, SSN);
				update.executeUpdate();
				
				request.getRequestDispatcher("employee/employeeManagement.jsp").forward(request, response);		
			}else if (action.equals("deleteEmployee")) {
				// delete an employee
				String SSN = (String)request.getParameter("SSN");

				//DELETE FROM EmployeeTable WHERE EmployeeTable.SSN = "123-45-6789";
				String query = "DELETE FROM EmployeeTable WHERE EmployeeTable.SSN = ?";
				PreparedStatement update = conn.prepareStatement(query);
				update = conn.prepareStatement(query);
				update.setString(1, SSN);
				update.executeUpdate();
				request.getRequestDispatcher("employee/employeeManagement.jsp").forward(request, response);	
			}
			else if (action.equals("addEmployeeInfo")) {
				int Id = Integer.valueOf(request.getParameter("Id"));
				String PasswordVal = request.getParameter("PasswordVal");
				String LastName = request.getParameter("LastName");
				String FirstName = request.getParameter("FirstName");
				String SSN = request.getParameter("SSN"); 
				String Address = request.getParameter("Address"); 
				int ZipCode = Integer.valueOf(request.getParameter("ZipCode"));
				String Telephone = request.getParameter("Telephone");
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				String StartDate = dateFormat.format(date);
				double HourlyRate = Double.valueOf(request.getParameter("HourlyRate"));
				String UserName = request.getParameter("UserName");
				
			
				String query = "insert into Person(SSN, LastName, FirstName, Address, ZipCode, Telephone, UserName, PasswordVal) "
						+ "Values (?, ?, ?, ? , ?, ?, ?, ?); ";
				
				PreparedStatement update = conn.prepareStatement(query);
				update = conn.prepareStatement(query);
				update.setString(1, SSN);
				update.setString(2, LastName);
				update.setString(3, FirstName);
				update.setString(4, Address);
				update.setInt(5, ZipCode);
				update.setString(6, Telephone);
				update.setString(7, UserName);
				update.setString(8, PasswordVal);
				update.executeUpdate();
				
				String query2 = "insert into EmployeeTable(Id, SSN, StartDate, HourlyRate) "
						+ "VALUES (?, ?, ?, ?);";
				
				PreparedStatement update2 = conn.prepareStatement(query2);
				update2 = conn.prepareStatement(query2);
				update2.setInt(1, Id);
				update2.setString(2, SSN);
				update2.setString(3, StartDate);
				update2.setDouble(4, HourlyRate);
				update2.executeUpdate();
				
				request.getRequestDispatcher("employee/employeeManagement.jsp").forward(request, response);
			}
			else if (action.equals("addStock")) {
				String StockSymbol = request.getParameter("StockSymbol"); 
				String CompanyName = request.getParameter("CompanyName");
				String StockType = request.getParameter("StockType");
				double PricePerShare = Double.valueOf(request.getParameter("PricePerShare")); 
				int NumShares = Integer.valueOf(request.getParameter("NumShares"));
				
				String query = "insert into StockTable(StockSymbol, CompanyName, StockType, PricePerShare, NumShares) "
						+ "Values (?, ?, ?, ? , ?); ";
				
				PreparedStatement update = conn.prepareStatement(query);
				update = conn.prepareStatement(query);
				update.setString(1, StockSymbol);
				update.setString(2, CompanyName);
				update.setString(3, StockType);
				update.setDouble(4, PricePerShare);
				update.setInt(5, NumShares);
				update.executeUpdate();
				request.getRequestDispatcher("stockManagement.jsp").forward(request, response);	
			}
			else if (action.equals("addCustomerInfo")) {
				String SSN = request.getParameter("SSN"); 
				String LastName = request.getParameter("LastName");
				String FirstName = request.getParameter("FirstName");
				String Address = request.getParameter("Address"); 
				int ZipCode = Integer.valueOf(request.getParameter("ZipCode"));
				String Telephone = request.getParameter("Telephone");
				String UserName = request.getParameter("UserName");
				String PasswordVal = request.getParameter("PasswordVal");
				
				String Email = request.getParameter("Email");
				String CreditCardNumber = request.getParameter("CreditCardNumber");
				double Rating = Double.valueOf(request.getParameter("Rating"));
				
				//String UserName = request.getParameter("UserName");
				
			
				String query = "insert into Person(SSN, LastName, FirstName, Address, ZipCode, Telephone, UserName, PasswordVal) "
						+ "Values (?, ?, ?, ? , ?, ?, ?, ?); ";
				
				PreparedStatement update = conn.prepareStatement(query);
				update = conn.prepareStatement(query);
				update.setString(1, SSN);
				update.setString(2, LastName);
				update.setString(3, FirstName);
				update.setString(4, Address);
				update.setInt(5, ZipCode);
				update.setString(6, Telephone);
				update.setString(7, UserName);
				update.setString(8, PasswordVal);
				update.executeUpdate();
				
				String query2 = "insert into ClientTable(Id, CreditCardNumber, Email, rating, active) "
						+ "VALUES (?, ?, ?, ?, ?);";
				
				PreparedStatement update2 = conn.prepareStatement(query2);
				update2 = conn.prepareStatement(query2);
				update2.setString(1, SSN);
				update2.setString(2, CreditCardNumber);
				update2.setString(3, Email);
				update2.setDouble(4, Rating);
				update2.setBoolean(5, true);
				update2.executeUpdate();
				
				String query3 = "insert into AccountTable(Id, DateOpened, ClientId) "
						+ "VALUES (?, ?, ?);";
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				String dateTimeStamp = dateFormat.format(date);
				
				PreparedStatement update3 = conn.prepareStatement(query3);
				update3.setInt(1, 1);
				update3.setString(2, dateTimeStamp);
				update3.setString(3, SSN);
				update3.executeUpdate();
				
				request.getRequestDispatcher("employee/employeeManagement.jsp").forward(request, response);
			}
		}catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			request.getRequestDispatcher("errorPage.jsp").forward(request, response);
		}
	}
}
