package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBC.Conn;
import beans.Stock;

/**
 * Servlet implementation class orderList
 */
@WebServlet("/orderList")
public class orderList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public orderList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String selectedStock = (String)request.getParameter("selectedStock");
		
		try {
			Connection conn = (Connection) Conn.getMySQLConnection();
			HttpSession session = request.getSession();
			ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

			Statement st = conn.createStatement();
			ResultSet result = st.executeQuery("Select * From OrderTable O Where O.StockName = @" + selectedStock);
			
			while(result.next()) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("Id", result.getString("Id"));
				map.put("NumShares", result.getString("NumShares"));
				map.put("DateTimeStamp", result.getString("DateTimeStamp"));
				map.put("stopAmount", result.getString("stopAmount"));
				map.put("PricePerShare", result.getString("PricePerShare"));
				map.put("Price", result.getString("Price"));
				map.put("OrderType", result.getString("OrderType"));
				list.add(map);
			}
				
			session.setAttribute("orderList", list);
			request.getRequestDispatcher("orderList.jsp").forward(request, response);
		} 
		catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
