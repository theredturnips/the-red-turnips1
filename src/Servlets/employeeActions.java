package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBC.Conn;
import beans.Stock;

/**
 * This servlet handles the four actions the employee can handle
 */
@WebServlet("/employeeActions")
public class employeeActions extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public employeeActions() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = (String)request.getParameter("hidden");
		
		try {
			Connection conn = (Connection) Conn.getMySQLConnection();
			HttpSession session = request.getSession();
			String userName = (String)session.getAttribute("userName");
			String pass = (String)session.getAttribute("password");
			ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

			if (action.equals("mailingList")) {
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery("SELECT P.LastName, P.FirstName, P.Address, P.ZipCode, C.Email FROM Person P, ClientTable C WHERE C.Id = P.SSN AND C.active=true");
				
				while(result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("last", result.getString("LastName"));
					map.put("first", result.getString("FirstName"));
					map.put("address", result.getString("Address"));
					map.put("zip", String.valueOf(result.getInt("ZipCode")));
					map.put("email", result.getString("Email"));
					list.add(map);
				}
				
				session.setAttribute("mailingList", list);
				request.getRequestDispatcher("employee/employeePage.jsp").forward(request, response);
				
			} else if (action.equals("suggestions")) {
				// Get The Client
				String clientID = request.getParameter("clientID");
				// first delete the suggestions for the client to refresh
				String query = "DELETE FROM StockSuggestion WHERE ClientId=" + "\"" + clientID + "\"";
				Statement st = conn.createStatement();
				st.executeUpdate(query);
				query = "SELECT S.StockSymbol, S.CompanyName, S.StockType, S.PricePerShare, S.NumShares from StockTable S, ClientTable C " +
								"where C.Id = ? and S.StockType IN (select S2.StockType from StockTable S2, StockPortfolio P " +
								"where P.ClientID = C.Id AND S2.StockSymbol = P.stockName);";		
				PreparedStatement pr = conn.prepareStatement(query);
				PreparedStatement insert = conn.prepareStatement("INSERT INTO StockSuggestion(ClientId,StockSymbol,CompanyName,StockType,PricePerShare,NumShares)"
						+ "VALUES(?,?,?,?,?,?)");
				pr.setString(1, clientID);
				ResultSet result = pr.executeQuery();
				int check = 0;
				String label = "";
				while (result.next()) {
					insert.setString(1, clientID);
					insert.setString(2, result.getString("StockSymbol"));
					insert.setString(3, result.getString("CompanyName"));
					insert.setString(4, result.getString("StockType"));
					insert.setDouble(5, result.getDouble("PricePerShare"));
					insert.setInt(6, result.getInt("NumShares"));
					insert.executeUpdate();
					try {
						label = "Suggestions List Creation Successful!";
					} catch (Exception e) {
						label = "Error already created list";
						break;
					}
					check++;
				}
				
				if (check == 0) {
					label = "No Suggestions Found!";
				}
				
				session.setAttribute("suggestionLabel", label);
				request.getRequestDispatcher("employee/employeePage.jsp").forward(request, response);
			} else if (action.equals("change")) {
				String info = (String)request.getParameter("test");
				// store data in dictionary and send it to the jsp page for editing
				HashMap<String, String> clientInfo = new HashMap<String, String>();
				int index = info.indexOf(",");
				String id = info.substring(0, index);
				info = info.substring(index + 1);
				index = info.indexOf(",");
				String creditCard = info.substring(0, index);
				info = info.substring(index + 1);
				index = info.indexOf(",");
				String email = info.substring(0, index);
				info = info.substring(index + 1);
				index = info.indexOf(",");
				String rating = info.substring(0, index);
				clientInfo.put("creditCard", creditCard);
				clientInfo.put("email", email);
				clientInfo.put("rating", rating);
				session.setAttribute("clientInfo", clientInfo);
				session.setAttribute("clientId", id);
				request.getRequestDispatcher("employee/updateClient.jsp").forward(request, response);
			} else if (action.equals("update")) {
				// update client information
				String id = (String)session.getAttribute("clientId");
				String creditCard = (String)request.getParameter("creditCard");
				String email = (String)request.getParameter("email");
				double rating = Double.valueOf((String)request.getParameter("rating"));
				String query = "UPDATE ClientTable SET CreditCardNumber = ?, Email = ?,"
						+ " rating = ? WHERE Id=?";
				PreparedStatement update = conn.prepareStatement(query);
				update = conn.prepareStatement(query);
				update.setString(1, creditCard);
				update.setString(2, email);
				update.setDouble(3, rating);
				update.setString(4, id);
				update.executeUpdate();
				String url = "login?userName=" + userName + "&password=" + pass + "&type=employee";
				request.getRequestDispatcher(url).forward(request, response);
			} else if (action.equals("record")) {
				// record a market order
				String orderId = (String)request.getParameter("check");
				String receipt = (String)request.getParameter(orderId);
				
				String query = "SELECT S.PricePerShare, O.NumShares, O.AccNum, O.StockSymbol, O.OrderType, O.ClientID FROM StockTable S, PendingOrder O WHERE O.StockSymbol = S.StockSymbol AND "
						+ "O.Id = " + orderId;
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery(query);
				double currentStockPrice = 0;
				int numShares = 0;
				int accNum = 0;
				String stockSymbol = "";
				String orderType = "";
				String brokerID = (String) request.getSession().getAttribute("brokerID");
				String clientID = "";
				while(result.next()){
					currentStockPrice = result.getDouble("PricePerShare");
					numShares = result.getInt("NumShares");
					accNum = result.getInt("AccNum");
					stockSymbol = result.getString("StockSymbol");
					orderType = result.getString("OrderType");
					clientID = result.getString("clientID");
				}	
				
				double fee = currentStockPrice * 0.05 * numShares;
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = new Date();
				String dateTimeStamp = dateFormat.format(date);
			
				if (receipt != null) {
					query = "INSERT INTO Receipt(DateTimeStamp, StockSymbol, NumShares, ClientID, OrderType) VALUES(?,?,?,?,?)";
					PreparedStatement prepared = conn.prepareStatement(query);
					prepared.setString(1, dateTimeStamp);
					prepared.setString(2, stockSymbol);
					prepared.setInt(3, numShares);
					prepared.setString(4, clientID);
					prepared.setString(5, orderType);
					prepared.executeUpdate();
				}
				
				query = "SELECT MAX(id) AS ID FROM TransactionTable";
				st = conn.createStatement();
				result = st.executeQuery(query);
				int id = 0;
				while (result.next()) {
					id = result.getInt("ID");				
				}
				
				id = id + 1; // next order 
				
				//TODO: Change to PreparedStatement
				query = "INSERT INTO TransactionTable (Id, Fee, DateTimeStamp, PricePerShare) VALUES "
						+ "(" + id + "," + fee + "," + "\"" + dateTimeStamp.toString() + "\""  + "," + currentStockPrice + ")";
				
				st.executeUpdate(query);

				query = "INSERT INTO TradeTable (ClientId, BrokerId, TransactionId, OrderId, StockId) VALUES "
						+ "(" + "\"" + clientID + "\"" + "," + brokerID + "," + id + "," + orderId + "," + "\"" + stockSymbol + "\"" + ")";
				
				st.executeUpdate(query);
				
				int accNumShares = 0;
				int stockNumShares = 0;
				
				// now update the accounts and stock amounts to reflect buying and selling of the specific number of shares
				// will have to move this up for error checking
				// increase numshares in account subtract from stock 
				query = "SELECT numShares FROM StockTable S WHERE StockSymbol=" + "\"" + stockSymbol + "\"";
				result = st.executeQuery(query);
				while (result.next()) {
					stockNumShares = result.getInt("numShares");
				}
				query = "SELECT P.numShares FROM StockPortfolio P WHERE P.Id=" + accNum + 
						" AND P.stockName= " + "\"" + stockSymbol + "\"" + " AND P.ClientId=" + "\"" + clientID + "\"";
				result = st.executeQuery(query);
				
				while (result.next()) {
					accNumShares = result.getInt("numShares");
				}	
				
				// check if client has this stock in their account 
				// check if client owns this stock in this account already if now, add new entry
				query = "SELECT COUNT(*) as count FROM StockPortfolio WHERE stockName=" + "\"" + stockSymbol + "\"" + " AND ClientId= " + "\"" + clientID + "\"" + " AND Id=" + accNum;
				result = st.executeQuery(query);
				int test = 0;
				while (result.next()) {
					test = result.getInt("count");
				}
				
				if (orderType.equalsIgnoreCase("buy")) {
					// add to account by num shares subtract from stock
					if (test == 1) {
						// they have the stock already so just add more
						query = "UPDATE StockPortfolio SET numShares=" + (accNumShares + numShares) + " WHERE stockName=" + "\"" + stockSymbol + "\"" + " AND ClientId= " + "\"" + clientID + "\"" + " AND Id=" + accNum; 
						st.executeUpdate(query);
					} else {
						// add new entry
						query = "INSERT INTO StockPortfolio(Id,ClientId,stockName,numShares) VALUES(?,?,?,?)";
						PreparedStatement pr = conn.prepareStatement(query);
						pr.setInt(1, accNum);
						pr.setString(2, clientID);
						pr.setString(3, stockSymbol);
						pr.setInt(4, numShares);
						pr.executeUpdate();
					}
					
					
					query = "UPDATE StockTable SET numShares=" + (stockNumShares - numShares) + " WHERE StockSymbol=" + "\"" + stockSymbol + "\"";
					st.executeUpdate(query);
				} else {
					// add to account by num shares subtract from stock
					query = "UPDATE StockPortfolio SET numShares=" + (accNumShares - numShares) + " WHERE stockName=" + "\"" + stockSymbol + "\"";
					st.executeUpdate(query);
					
					if ((accNumShares - numShares) <= 0) {
						// remove this entry from the account table as they sold all their shares
						query = "DELETE FROM StockPortfolio WHERE stockName=" + "\"" + stockSymbol + "\"" + " AND ClientId= " + "\"" + clientID + "\"" + " AND Id=" + accNum;
						st.executeUpdate(query);
					}
					
					query = "UPDATE StockTable SET numShares=" + (stockNumShares + numShares) + " WHERE StockSymbol=" + "\"" + stockSymbol + "\"";
					st.executeUpdate(query);
				}
				
				// remove order from pending order list
				query = "DELETE FROM PendingOrder WHERE Id=" + orderId;
				st.executeUpdate(query);
				
				ArrayList<HashMap<String, String>> orderList = new ArrayList<HashMap<String, String>>();
				Statement st1 = conn.createStatement();
				query = "SELECT * FROM PendingOrder";
				result = st1.executeQuery(query);
				while(result.next()){
					HashMap<String, String> map = new HashMap<String, String>();
					String str = result.getString("PriceType");
					
					// Do not add the conditional orders to the list, they must remain hidden until executed
					if (result.getString("PriceType").equals("Trailing Stop") || result.getString("PriceType").equals("Hidden Stop")) {
						continue;
					}
					
					map.put("orderId", String.valueOf(result.getString("Id")));
					map.put("numShares", String.valueOf(result.getInt("NumShares")));
					map.put("dateTimeStamp", result.getString("DateTimeStamp"));
					map.put("clientID", result.getString("ClientID"));
					map.put("accNum", result.getString("AccNum"));
					map.put("stockSymbol", result.getString("StockSymbol"));
					map.put("priceType", result.getString("PriceType"));
					map.put("orderType", result.getString("OrderType"));
					orderList.add(map);
				}
				session.setAttribute("orders", orderList);
				request.getRequestDispatcher("/employee/employeePage.jsp").forward(request, response);
			} else if (action.equals("seeEmployees")) {
				ArrayList<HashMap<String, String>> employeeList = new ArrayList<HashMap<String, String>>();
				String query = "select * FROM EmployeeTable E, Person P WHERE E.SSN = P.SSN";
				Statement st = conn.createStatement();
				ResultSet result = st.executeQuery(query);
				while (result.next()) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Id", String.valueOf(result.getInt("Id")));
					map.put("StartDate",  result.getString("StartDate"));
					map.put("LastName", result.getString("LastName"));
					map.put("FirstName", result.getString("FirstName"));
					map.put("Telephone", String.valueOf(result.getInt("Telephone")));
					employeeList.add(map);
				}
				session.setAttribute("employeeList", employeeList);
				request.getRequestDispatcher("/employee/seeEmployees.jsp").forward(request, response);
			}
				
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			request.getRequestDispatcher("errorPage.jsp").forward(request, response);
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
