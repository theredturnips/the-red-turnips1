package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBC.Conn;
import beans.Client;

/**
 * For logging in, must check what type of user tries to log in
 */
@WebServlet("/login")
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public login() {
		super();
	}

	@SuppressWarnings("resource")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Get Connection
		try {			
			Connection conn = (Connection) Conn.getMySQLConnection();

			String userName = (String)request.getParameter("userName");
			String password = (String)request.getParameter("password");
			String type = (String) request.getParameter("type");
			
			HttpSession session = request.getSession();
			session.setAttribute("userName", userName);
			session.setAttribute("password", password);
			session.setAttribute("type", type);
			
			if (type.equals("employee")) {
				String query = "SELECT Count(*) AS test FROM Person P, EmployeeTable E WHERE P.UserName = ? AND P.PasswordVal = ? AND E.SSN = (SELECT SSN FROM Person WHERE UserName = ?)"; 
				PreparedStatement pr = conn.prepareStatement(query);
				pr.setString(1, userName);
				pr.setString(2, password);
				pr.setString(3, userName);

				System.out.println(pr);
				ResultSet result = pr.executeQuery();
				int val = -1;
				while (result.next()) {
					val = result.getInt("test");
				}
				// search person for value
				//TODO: does dWarren have to be manager?
				if (val == 1 && userName.equals("dWarren")) {
					// redirect to manager page
					request.getRequestDispatcher("stockManagement.jsp").forward(request, response);
				} else if (val == 1) {
					// redirect to employee page, first send the client data to edit
					ArrayList<Client> clientList = new ArrayList<Client>();
					Statement st = conn.createStatement();
					result = st.executeQuery("SELECT * FROM ClientTable WHERE active=true");
					while (result.next()) {
						Client client = new Client();
						client.setID(result.getString("Id"));
						client.setCreditCardNumber(result.getString("CreditCardNumber"));
						client.setEmail(result.getString("email"));
						client.setRating(result.getDouble("rating"));
						clientList.add(client);
					}
					
					query = "SELECT E.Id From EmployeeTable E, Person P WHERE P.UserName=" + "\"" + userName + "\"" + " AND E.SSN = P.SSN";
					result = st.executeQuery(query);
					String brokerID = "";
					while(result.next()) {
						brokerID = result.getString("Id");
					}
					
					ArrayList<HashMap<String, String>> orderList = new ArrayList<HashMap<String, String>>();
					Statement st1 = conn.createStatement();
					query = "SELECT * FROM PendingOrder";
					result = st1.executeQuery(query);
					while(result.next()){
						HashMap<String, String> map = new HashMap<String, String>();
						String str = result.getString("PriceType");
						
						// Do not add the conditional orders to the list, they must remain hidden until executed
						if (result.getString("PriceType").equals("Trailing Stop") || result.getString("PriceType").equals("Hidden Stop")) {
							continue;
						}
						
						map.put("orderId", String.valueOf(result.getString("Id")));
						map.put("numShares", String.valueOf(result.getInt("NumShares")));
						map.put("dateTimeStamp", result.getString("DateTimeStamp"));
						map.put("clientID", result.getString("ClientID"));
						map.put("accNum", result.getString("AccNum"));
						map.put("stockSymbol", result.getString("StockSymbol"));
						map.put("priceType", result.getString("PriceType"));
						map.put("orderType", result.getString("OrderType"));
						orderList.add(map);
					}
					
					session.setAttribute("clients", clientList);
					session.setAttribute("orders", orderList);
					session.setAttribute("brokerID", brokerID);
					request.getRequestDispatcher("employee/employeePage.jsp").forward(request, response);
				} else {
					// go back to login page say "invalid username or password"
					request.getRequestDispatcher("login.jsp").forward(request, response);
				}

			} else if (type.equals("client")) {
				String query = "SELECT Count(*) AS test FROM Person P, ClientTable C WHERE P.UserName = ? AND P.PasswordVal = ? AND C.Id = (SELECT SSN FROM Person WHERE UserName = ?) AND C.active=true"; 
				PreparedStatement pr = conn.prepareStatement(query);
				pr.setString(1, userName);
				pr.setString(2, password);
				pr.setString(3, userName);
				System.out.println(pr);
				ResultSet result = pr.executeQuery();
				int val = -1;
				while (result.next()) {
					val = result.getInt("test");
				}
				if(val == 1){
					ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
					Statement st = conn.createStatement();
					query = "SELECT A.stockName, A.numShares,  A.Id  FROM Person P, StockPortfolio A "
							+ "WHERE P.UserName = " + "\"" + userName + "\"" +" AND " + " P.SSN = A.ClientId";
					result = st.executeQuery(query);
					while(result.next()){
						HashMap<String, String> map = new HashMap<String, String>();
						map.put("stock", result.getString("stockName"));
						map.put("numShares", result.getString("numShares"));
						map.put("account", String.valueOf(result.getInt("Id")));
						list.add(map);
					}
					
					query = "SELECT * FROM OrderTable WHERE ClientID=(SELECT SSN FROM Person WHERE UserName=" + "\"" + userName + "\"" +")";
					result = st.executeQuery(query);
					ArrayList<HashMap<String, String>> trailingHistoryList = new ArrayList<HashMap<String, String>>();
					ArrayList<HashMap<String, String>> hiddenHistoryList = new ArrayList<HashMap<String, String>>();
					while(result.next()) {
						HashMap<String, String> map = new HashMap<String, String>();
						String str = result.getString("PriceType");
						if (result.getString("PriceType").equals("Trailing Stop")) {
							map.put("orderId", result.getString("Id"));
							map.put("StockSymbol", result.getString("StockSymbol"));
							map.put("NumShares", result.getString("NumShares"));
							trailingHistoryList.add(map);
						} else if (result.getString("PriceType").equals("Hidden Stop")) {
							map.put("orderId", result.getString("Id"));
							map.put("StockSymbol", result.getString("StockSymbol"));
							map.put("NumShares", result.getString("NumShares"));
							hiddenHistoryList.add(map);
						}
					}
					
					query = "SELECT StockSymbol, StockType FROM StockTable";
					result = st.executeQuery(query);
					ArrayList<HashMap<String, String>> symbols = new ArrayList<HashMap<String, String>>();
					ArrayList<String> types = new ArrayList();
					while(result.next()) {
						HashMap<String, String> map = new HashMap<String, String>();
						map.put("stockSymbol", result.getString("StockSymbol"));
						types.add(result.getString("StockType"));
						symbols.add(map);
					}
					
					query = "SELECT * FROM AccountTable WHERE ClientID=(SELECT SSN FROM Person WHERE username=" + "\"" + userName + "\"" + ")";;
					result = st.executeQuery(query);
					ArrayList<Integer> accNum = new ArrayList();
					while(result.next()) {
						accNum.add(result.getInt("Id"));
					}
					
					session.setAttribute("accNum", accNum);
					session.setAttribute("stockTypes", types);
					session.setAttribute("stocklist", list);
					session.setAttribute("userName", userName);
					session.setAttribute("trailingList", trailingHistoryList);
					session.setAttribute("hiddenList", hiddenHistoryList);
					session.setAttribute("stockSymbol", symbols);
					request.getRequestDispatcher("customer/customerPage.jsp").forward(request, response);
				} else {
					//invalid username or password
					request.getRequestDispatcher("/login.jsp").forward(request, response);
				}

			} else {
				// choose value
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			}


	}catch(Exception e)
	{
		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
