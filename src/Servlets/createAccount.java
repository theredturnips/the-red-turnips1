package Servlets;

import JDBC.Conn;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;

/**
 * Servlet implementation class createAccount
 */
@WebServlet("/createAccount")
public class createAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public createAccount() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO: CHANGE CLIENT TO ALSO HAVE SSN AS POINTER TO PERSON SO ID ISNT SAME AS SSN
		// TODO: Make work for null values
		// Create person, then create client associated with that person
		String insertPerson = "INSERT INTO Person (SSN, LastName, FirstName, Address, ZipCode, Telephone, PasswordVal)"
				+ " VALUES (?,?,?,?,?,?,?)";
		String insertClient = "INSERT INTO ClientTable (Id, CreditCardNumber, Email, rating) VALUES (?,?,?,?)";
		
		try {
			Connection conn = (Connection) Conn.getMySQLConnection();
			// first create a person with the values
			PreparedStatement st = conn.prepareStatement(insertPerson);
			st.setString(1, request.getParameter("SSN"));
			st.setString(2, request.getParameter("lastname"));
			st.setString(3, request.getParameter("firstname"));
			st.setString(4, request.getParameter("address"));
			st.setInt(5, Integer.parseInt(request.getParameter("zip")));
			st.setInt(6, Integer.parseInt(request.getParameter("telephone")));
			st.setString(7, request.getParameter("password"));
			// update database
			st.executeUpdate();
			
			// now create the client
			st = conn.prepareStatement(insertClient);
			st.setString(1, request.getParameter("SSN"));
			st.setString(2, request.getParameter("credicard"));
			st.setString(3, request.getParameter("email"));
			st.setInt(4, 0); // 0 by default
			
			st.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
