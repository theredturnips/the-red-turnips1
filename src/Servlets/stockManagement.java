package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBC.Conn;
import beans.Stock;

/**
 * Servlet implementation class stockManagement
 */
@WebServlet("/stockManagement")
public class stockManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public stockManagement() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO USE SESSION?
		// get all stocks and put in list

		ArrayList<Stock> list = new ArrayList<Stock>();
		try {
			Connection conn = (Connection) Conn.getMySQLConnection();
			String query = "Select * From StockTable";
			Statement st = conn.createStatement();
			ResultSet r = st.executeQuery(query);
			
			while (r.next()) {
				Stock stock = new Stock();
				stock.setStockSymbol(r.getString("StockSymbol"));
				stock.setCompanyName(r.getString("CompanyName"));
				stock.setStockType(r.getString("StockType"));
				stock.setPricePerShare(r.getDouble("PricePerShare"));
				stock.setNumShares(r.getInt("NumShares"));
				list.add(stock);
			}
	
			HttpSession session = request.getSession();
			session.setAttribute("stock", list);
		
			request.getRequestDispatcher("/stockManagement.jsp").forward(request, response);
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
