package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conn {
	  public static Connection getMySQLConnection()
	          throws ClassNotFoundException, SQLException {

		  Class.forName("com.mysql.jdbc.Driver");
	      Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", 
	    		  "root", "jl8141994");
	      return conn;
	  }
	
}
