DROP DATABASE IF EXISTS db;

CREATE DATABASE db;

USE db;

DROP TABLE IF EXISTS Location, Person, EmployeeTable, ClientTable, AccountTable, TransactionTable, OrderTable, StockTable, TradeTable;

create table Location(
        ZipCode INTEGER,
    City CHAR(20) NOT NULL,
    State CHAR(20) NOT NULL,
    PRIMARY KEY(ZipCode)
);
CREATE TABLE Person (
        SSN CHAR(20),
        LastName CHAR(20) NOT NULL,
        FirstName CHAR(20) NOT NULL,
        Address CHAR(20),
        ZipCode INTEGER,
        Telephone INTEGER,
    UserName Char(20),
    PasswordVal Char(20),
        PRIMARY KEY(SSN)
);
create table EmployeeTable(
        Id integer,
    SSN CHAR(20),
    StartDate DATE,
    HourlyRate INTEGER,
    PRIMARY KEY(Id),
    FOREIGN KEY (SSN) REFERENCES Person(SSN)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
);

create table ClientTable(
	Id CHAR(20),
    CreditCardNumber CHAR(20),
    Email CHAR(32),
    rating double,
    primary key(Id),
    foreign key(Id) references Person(SSN)
    on delete cascade
    on update cascade
);
create table StockTable(
	StockSymbol CHAR(20),
    CompanyName char(20) not null,
    StockType char(20) not null,
    PricePerShare double,
    NumShares integer,
    primary key(StockSymbol)
);

CREATE TABLE StockHistory(
	StockSymbol CHAR(20),
    PricePerShare double,
    DateTimeStamp CHAR(20),
    PRIMARY KEY(StockSymbol, DateTimeStamp, PricePerShare),
    FOREIGN KEY(StockSymbol) REFERENCES StockTable(StockSymbol)
);

CREATE TABLE StockSuggestion(
	ClientId CHAR(20),
    StockSymbol CHAR(20),
    CompanyName CHAR(20),
    StockType CHAR(20),
    PricePerShare double,
    NumShares integer,
    Primary key(ClientId, StockSymbol),
    FOREIGN KEY(StockSymbol) REFERENCES StockTable(StockSymbol),
    FOREIGN KEY(ClientId) REFERENCES ClientTable(Id)
);

create table AccountTable(
	Id integer,
    DateOpened CHAR(20),
    ClientId CHAR(20),
    primary key (Id, ClientId),
    foreign key(ClientId) references ClientTable(Id)
    on delete cascade
	on update cascade
);

create table StockPortfolio(
	Id integer,
    ClientId CHAR(20),
    stockName Char(20),
    numShares INTEGER,
    primary key (Id,ClientId, stockName),
    foreign key(ClientId) references AccountTable(ClientId),
    FOREIGN KEY(stockName) REFERENCES StockTable(StockSymbol)
);

create table TransactionTable(
	Id integer,
    Fee double,
    DateTimeStamp datetime,
    PricePerShare double,
    primary key (Id)
);

create table OrderTable(
	Id integer,
	NumShares integer,
    DateTimeStamp datetime,
    stopValue CHAR(20),
    ClientID CHAR(20),
    AccNum Char(20),
    StockSymbol CHAR(20),
    PriceType CHAR(20),
    OrderType enum('Buy', 'Sell'),
    primary key(Id, DateTimeStamp)
);

create table Receipt(
	DateTimeStamp CHAR(20),
    StockSymbol CHAR(20),
    NumShares integer,
    ClientID Char(20),
    OrderType enum('Buy', 'Sell'),
    Primary Key(ClientID, DateTimeStamp),
    FOREIGN KEY (ClientID) References ClientTable(Id)
);

create table PendingOrder(
	Id integer,
	NumShares integer,
    DateTimeStamp datetime,
    stopValue CHAR(20),
    ClientID CHAR(20),
    AccNum Char(20),
    StockSymbol CHAR(20),
    PriceType CHAR(20),
    OrderType enum('Buy', 'Sell'),
    primary key(Id, DateTimeStamp),
    foreign key(Id) References OrderTable(Id)
);

CREATE TABLE ConditionalHistoryTable(
	OrderId integer,
    PricePerShare double,
    StopAmount double,
    DateTimeStamp datetime,
    PriceType CHAR(20),
    stopValue Char(10),
    Sold CHAR(1),
    primary key(OrderId, DateTimeStamp),
    foreign key (OrderId) REFERENCES OrderTable (Id)
);

create table TradeTable(
    AccountId integer,
	BrokerId integer,
	TransactionId integer,
	OrderId integer,
	StockId CHAR(20),
    PRIMARY KEY (AccountId, BrokerId, TransactionId, OrderId, StockId),
        FOREIGN KEY (AccountId) REFERENCES AccountTable (Id)
                ON DELETE NO ACTION
                ON UPDATE CASCADE,
        FOREIGN KEY (BrokerId) REFERENCES EmployeeTable (Id)
                ON DELETE NO ACTION
                ON UPDATE CASCADE,
        FOREIGN KEY (TransactionId) REFERENCES TransactionTable (Id)
                ON DELETE NO ACTION
                ON UPDATE CASCADE,
        FOREIGN KEY (OrderId) REFERENCES OrderTable (Id)
                ON DELETE NO ACTION
                ON UPDATE CASCADE,
        FOREIGN KEY (StockId) REFERENCES StockTable (StockSymbol)
                ON DELETE NO ACTION
                ON UPDATE CASCADE
);

insert into Location(ZipCode, City, State) Values(11790, "Stony Brook", "NY");

insert into Person(SSN, LastName, FirstName, Address, ZipCode, Telephone, UserName, PasswordVal) Values ("111-11-1111", "Yang", "Shang",
"123 Success Street", 11790, 516, "sYang", "abc");
insert into Person(SSN, LastName, FirstName, Address, ZipCode, Telephone, UserName, PasswordVal) Values ("222-22-2222", "Du", "Victor",
"456 Fortune Road", 11790, 516, "vDu", "123");
insert into Person(SSN, LastName, FirstName, Address, ZipCode, Telephone, UserName, PasswordVal) Values ("333-33-3333", "Smith", "John",
"789 Peace Blvd.", 11790, 516, "jSmith", "password");
insert into Person(SSN, LastName, FirstName, Address, ZipCode, Telephone, UserName, PasswordVal) Values ("444-44-4444", "Phillip", "Lewis",
"135 Knowledge Lane", 11790, 123, "lPhillip", "pass");
insert into Person(SSN, LastName, FirstName, Address, ZipCode, Telephone, UserName, PasswordVal) Values ("123-45-6789", "Smith", "David",
"123 College road", 11790, 5555555, "dSmith", "pass");
insert into Person(SSN, LastName, FirstName, Address, ZipCode, Telephone, UserName, PasswordVal) Values ("789-12-3456", "Warren", "David",
"456 Sunken Street", 11790, 5165555, "dWarren", "pass");

insert into ClientTable(Id, CreditCardNumber, Email, rating) VALUES
    ("111-11-1111", "1234-5678-1234-5678", "syang@cs.sunysb.edu", 1);
insert into ClientTable(Id, CreditCardNumber, Email, rating) VALUES
    ("222-22-2222", "5678-1234-5678-1234", "vicdu@cs.sunysb.edu", 1);
insert into ClientTable(Id, CreditCardNumber, Email, rating) VALUES
    ("333-33-3333", "2345-6789-2345-6789", "jsmith@cs.sunysb.edu", 1);
insert into ClientTable(Id, CreditCardNumber, Email, rating) VALUES
    ("444-44-4444", "6789-2345-6789-2345", "pml@cs.sunysb.edu", 1);

insert into EmployeeTable(Id, SSN, StartDate, HourlyRate) VALUES
    (1, "123-45-6789", '2005-01-11', 60);
insert into EmployeeTable(Id, SSN, StartDate, HourlyRate) VALUES
    (2, "789-12-3456", '2006-02-06', 50);

insert into StockTable(StockSymbol, CompanyName, StockType, PricePerShare, NumShares) Values
 ("GM", "General Motors", "automotive", 34.23, 1000);
insert into StockTable(StockSymbol, CompanyName, StockType, PricePerShare, NumShares) Values
("IBM", "IBM", "computer", 91.41, 500);
insert into StockTable(StockSymbol, CompanyName, StockType, PricePerShare, NumShares) Values
("F", "Ford", "automotive", 9.0, 750);

insert into StockHistory(StockSymbol, PricePerShare, DateTimeStamp) Values ("GM", 34.23, '21-02-2016');
insert into StockHistory(StockSymbol, PricePerShare, DateTimeStamp) Values ("IBM", 91.41, '21-04-2016');
insert into StockHistory(StockSymbol, PricePerShare, DateTimeStamp) Values ("F", 9.0, '21-04-2016');

insert into AccountTable(Id, DateOpened, ClientId) VALUES
(1, "2006-10-15", "444-44-4444");
insert into AccountTable(Id, DateOpened, ClientId) VALUES
(1, "2006-01-10", "222-22-2222");

insert into StockPortfolio(Id, ClientId, stockName, numShares) VALUES
(1, "444-44-4444", "GM", 250);
insert into StockPortfolio(Id, ClientId, stockName, numShares) VALUES
(1, "444-44-4444", "F", 100);
insert into StockPortfolio(Id, ClientId, stockName, numShares) VALUES
(1, "222-22-2222", "IBM", 50);

insert into OrderTable(Id, NumShares, DateTimeStamp, ClientID, AccNum, StockSymbol, stopValue, PriceType, OrderType) VALUES
        (1, 75, "2010-01-10 12:12:12", "444-44-4444", 1, "GM", null, "Market", "Buy");
insert into OrderTable(Id, NumShares, DateTimeStamp, ClientID, AccNum, StockSymbol, stopValue, PriceType, OrderType) VALUES
        (2, 10, "2010-01-11 12:12:12", "222-22-2222", 1, "IBM", "10%", "Trailing Stop", "Sell");
insert into OrderTable(Id, NumShares, DateTimeStamp, ClientID, AccNum, StockSymbol, stopValue, PriceType, OrderType) VALUES
        (2, 10, "2010-02-11 12:12:12", "222-22-2222", 1, "IBM", null,  "$90","Sell");

insert into PendingOrder(Id, NumShares, DateTimeStamp, ClientID, AccNum, StockSymbol, stopValue, PriceType, OrderType) VALUES
        (1, 75, "2010-01-10 12:12:12", "444-44-4444", 1, "GM", null, "Market", "Buy");
insert into PendingOrder(Id, NumShares, DateTimeStamp, ClientID, AccNum, StockSymbol, stopValue, PriceType, OrderType) VALUES
        (2, 10, "2010-02-11 12:12:12", "222-22-2222", 1, "IBM", null, "$90","Sell");

insert into ConditionalHistoryTable(OrderId, PricePerShare, StopAmount, DateTimeStamp, PriceType, stopValue, Sold) VALUES
        (2, 91.41, 82.2, "2010-01-11 12:12:12", "Trailing Stop", "10%", "t");
insert into ConditionalHistoryTable(OrderId, PricePerShare, StopAmount, DateTimeStamp, PriceType, stopValue, Sold) VALUES
        (2, 90, 90, "2010-01-12 12:12:12", "Trailing Stop", null, "t");